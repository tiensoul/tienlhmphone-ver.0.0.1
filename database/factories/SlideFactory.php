<?php

use Faker\Generator as Faker;

$factory->define(App\Slide::class, function (Faker $faker) {
    return [
        'link' => '#',
        'image' => 'source/images/slide/slide(' . rand(1,7) .').png',
        'banner' => 'source/images/398x110/image' . rand(1,3) . '.png' //$faker->imageUrl($width = 398, $height = 110, 'nature')
    ];
});
