<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'id_type' => rand(1,10),
        'description' => $faker->paragraph($nbSentences = 8),
        'unit_price' => rand(1000,1000000000),
        'promotion_price' => rand(1000,1000000000),
        'image' => $faker->imageUrl($width = 400, $height = 400, 'technics'),
        'top' => rand(0,1),
        'unit' => rand(1,1000)
    ];
});
