<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\ProductType;
use App\Product;
use App\Cart;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\User;
use Hash;
use Session;
use Auth;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function getLoginAdmin()
    {
        return view('admin/login/index');
    }

    public function postLoginAdmin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'password' => 'required|max:20'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Địa chỉ email tối đa 60 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu tối đa 20 kí tự'
        ]);

        $info = array('email' => $request->email, 'password' => $request->password, 'role' => 1);
        if(Auth::attempt($info)) {
            return redirect('admin/trangchu')->with(['flag' => 'success', 'message' => 'Đăng nhập thành công!', 'check' => "TRUE"]);
        } else {
            return redirect('admin/login')->with(['flag' => 'danger', 'message' => 'Đăng nhập không thành công vui lòng kiểm tra lại!']);
        }
    }

    public function getDangXuatAdmin()
    {
        Auth::logout();
        return redirect('admin/login');
    }


    public function getIndex()
    {
        $products = Product::all();
        $bills_detail = BillDetail::all();
        $customers = Customer::all();
        $type_products = ProductType::all();
        return view('admin/trangchu', compact('products', 'bills_detail', 'customers', 'type_products'));
    }

    public function getSanPham()
    {
        $get_all_product = ProductType::join('products', 'type_products.id', '=', 'products.id_type')->select('products.id AS id_product', 'products.name AS name_product', 'products.short_description AS short_description_product', 'products.long_description AS long_description_product', 
        'products.unit_price AS unit_price', 'products.promotion_price AS promotion_price', 'products.image AS image', 'products.top AS top_product', 'type_products.name AS name_type_product')->orderBy('products.id', 'desc')->get();
        //dd($get_all_product);
        $get_all_type_products = ProductType::all();
        return view('admin/sanpham', compact('get_all_product', 'get_all_type_products'));
    }

    public function requestSuaSanPham(Request $request)
    {
        $product_edit = Product::find($request->idsanpham);
        return $product_edit->toJson();
    }

    public function postSuaSanPham(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'giagocsanpham' => 'required|numeric',
            'giakhuyenmaisanpham' => 'numeric',
            'motachitiet' => 'required'
        ],
        [
            'giagocsanpham.required' => 'Giá sản phẩm không được bỏ trống',
            'giagocsanpham.numeric' => 'Giá sản phẩm phải là số',
            'giakhuyenmaisanpham.numeric' => 'Giá khuyến mãi phải là số',
            'motachitiet.required' => 'Mô tả chi tiết không được để trống'
        ]);

        $name = 'hinhanhsua1';
        $name1 = 'hinhanhsua2';
        $name2 = 'hinhanhsua3';
        $name3 = 'hinhanhsua4';

        $flag1 = false;
        $flag2 = false;
        $flag3 = false;
        $flag4 = false;

        if ($request->file($name)){
            // Lấy tên file
            $file_name = $request->file($name)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name)->move('source/images',$file_name);
            $flag1 = true;
        }

        if ($request->hasFile($name1)){
            // Lấy tên file
            $file_name1 = $request->file($name1)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name1)->move('source/images',$file_name1);
            $flag2 = true;
        }

        if ($request->hasFile($name2)){
            // Lấy tên file
            $file_name2 = $request->file($name2)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name2)->move('source/images',$file_name2);
            $flag3 = true;
        }

        if ($request->hasFile($name3)){
            // Lấy tên file
            $file_name3 = $request->file($name3)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name3)->move('source/images',$file_name3);
            $flag4 = true;
        }
        $post_edit_product = Product::find($request->idsanpham);
        $post_edit_product->name = $request->tensanpham;
        $post_edit_product->id_type = $request->loaisanpham;
        $post_edit_product->short_description = $request->motasanpham;
        $post_edit_product->long_description = $request->motachitiet;
        $post_edit_product->unit_price = $request->giagocsanpham;
        $post_edit_product->promotion_price = $request->giakhuyenmaisanpham;
        $post_edit_product->image = 'img';
        $post_edit_product->top = $request->sanphamnoibat;
        $post_edit_product->save();

        if($flag1)
            $post_edit_product->image = 'source/images/'. $file_name;
        if($flag2)
            $post_edit_product->image1 = 'source/images/' . $file_name1;
        if($flag3)
            $post_edit_product->image2 = 'source/images/'. $file_name2;
        if($flag4)
            $post_edit_product->image3 = 'source/images/'. $file_name3;

        $post_edit_product->save();
        
        return redirect()->back()->with('thongbao', 'Cập nhật sản phẩm thành công!');
    }

    public function postThemSanPham(Request $request)
    {
        //dd($request->all());

        $this->validate($request, [
            'giagocsanphamthem' => 'required|numeric',
            'giakhuyenmaisanphamthem' => 'numeric',
            'motachitietthem' => 'required'
        ],
        [
            'giagocsanphamthem.required' => 'Giá sản phẩm không được bỏ trống',
            'giagocsanphamthem.numeric' => 'Giá sản phẩm phải là số',
            'giakhuyenmaisanphamthem.numeric' => 'Giá khuyến mãi phải là số',
            'motachitietthem.required' => 'Mô tả chi tiết không được để trống'
        ]);

        $name = 'hinhanhthem1';
        $name1 = 'hinhanhthem2';
        $name2 = 'hinhanhthem3';
        $name3 = 'hinhanhthem4';

        $messages = [
            'required' => 'Hình ảnh không được để trống, cần tải lên ít nhất một hình ảnh đầu tiên',
            'image' => 'Định dạng không cho phép',
            'max' => 'Kích thước file quá lớn',
        ];
        // Điều kiện cho phép upload
        $this->validate($request, [
            'hinhanhthem1' => 'required|image|max:50028',
        ], $messages);

        $flag1 = false;
        $flag2 = false;
        $flag3 = false;
        $flag4 = false;

        if ($request->file($name)->isValid()){
            // Lấy tên file
            $file_name = $request->file($name)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name)->move('source/images',$file_name);
            $flag1 = true;
        }

        if ($request->hasFile($name1)){
            // Lấy tên file
            $file_name1 = $request->file($name1)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name1)->move('source/images',$file_name1);
            $flag2 = true;
        }

        if ($request->hasFile($name2)){
            // Lấy tên file
            $file_name2 = $request->file($name2)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name2)->move('source/images',$file_name2);
            $flag3 = true;
        }

        if ($request->hasFile($name3)){
            // Lấy tên file
            $file_name3 = $request->file($name3)->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $request->file($name3)->move('source/images',$file_name3);
            $flag4 = true;
        }

        $post_add_product = new Product();
        $post_add_product->name = $request->tensanphamthem;
        $post_add_product->id_type = $request->loaisanphamthem;
        $post_add_product->short_description = $request->motasanphamthem;
        $post_add_product->long_description = $request->motachitietthem;
        $post_add_product->unit_price = $request->giagocsanphamthem;
        $post_add_product->promotion_price = $request->giakhuyenmaisanphamthem;
        $post_add_product->top = $request->sanphamnoibatthem;

        if($flag1)
            $post_add_product->image = 'source/images/'. $file_name;
        if($flag2)
            $post_add_product->image1 = 'source/images/' . $file_name1;
        if($flag3)
            $post_add_product->image2 = 'source/images/'. $file_name2;
        if($flag4)
            $post_add_product->image3 = 'source/images/'. $file_name3;

        $post_add_product->save();
        
        return redirect()->back()->with('thongbao', 'Thêm sản phẩm thành công!');
    }

    public function getSetNoiBat($id, $top)
    {
        $product_set_top = Product::find($id);
        if($top == 1)
        {
            $product_set_top->top = 0;
            $product_set_top->save();
            return redirect()->back();
        } else {
            $product_set_top->top = 1;
            $product_set_top->save();
            return redirect()->back();
        }
    }

    public function postXoaSanPham(Request $request)
    {
        $post_delete_product = Product::find($request->idsanphamcanxoa)->delete();
        return redirect()->back()->with('thongbao', 'Xóa sản phẩm thành công!');
    }

    public function getDonHang()
    {
        $bill_details = BillDetail::join('bills', 'bill_detail.id_bill', '=', 'bills.id')
                                    ->join('customer', 'bills.id_customer', '=', 'customer.id')
                                    ->join('products', 'bill_detail.id_product', '=', 'products.id')
                                    ->select('bills.id as bills_id', 'bills.id_customer as bills_id_customer',
                                    'bills.date_order as bills_date_order', 'bills.total as bills_total',
                                    'bills.total_promotion as bills_total_promotion', 'bills.total_payment as bills_total_payment',
                                    'bills.payment as  bills_payment', 'bills.note as bills_note', 'bill_detail.id as bill_detail_id',
                                    'bill_detail.id_bill as bill_detail_id_bill', 'bill_detail.id_product as bill_detail_id_product',
                                    'bill_detail.quantity as bill_detail_quantity', 'bill_detail.unit_price as bill_detail_unit_price',
                                    'customer.name as customer_name', 'customer.address as customer_address', 'products.name as products_name')
                                    ->where('bill_detail.confirm', 0)->get();
        $bill_details_proccess = BillDetail::join('bills', 'bill_detail.id_bill', '=', 'bills.id')
                                    ->join('customer', 'bills.id_customer', '=', 'customer.id')
                                    ->join('products', 'bill_detail.id_product', '=', 'products.id')
                                    ->select('bills.id as bills_id', 'bills.id_customer as bills_id_customer',
                                    'bills.date_order as bills_date_order', 'bills.total as bills_total',
                                    'bills.total_promotion as bills_total_promotion', 'bills.total_payment as bills_total_payment',
                                    'bills.payment as  bills_payment', 'bills.note as bills_note', 'bill_detail.id as bill_detail_id',
                                    'bill_detail.id_bill as bill_detail_id_bill', 'bill_detail.id_product as bill_detail_id_product',
                                    'bill_detail.quantity as bill_detail_quantity', 'bill_detail.unit_price as bill_detail_unit_price',
                                    'customer.name as customer_name', 'customer.address as customer_address', 'products.name as products_name')
                                    ->where('bill_detail.confirm', 1)->get();
        $bill_details_success = BillDetail::join('bills', 'bill_detail.id_bill', '=', 'bills.id')
                                    ->join('customer', 'bills.id_customer', '=', 'customer.id')
                                    ->join('products', 'bill_detail.id_product', '=', 'products.id')
                                    ->select('bills.id as bills_id', 'bills.id_customer as bills_id_customer',
                                    'bills.date_order as bills_date_order', 'bills.total as bills_total',
                                    'bills.total_promotion as bills_total_promotion', 'bills.total_payment as bills_total_payment',
                                    'bills.payment as  bills_payment', 'bills.note as bills_note', 'bill_detail.id as bill_detail_id',
                                    'bill_detail.id_bill as bill_detail_id_bill', 'bill_detail.id_product as bill_detail_id_product',
                                    'bill_detail.quantity as bill_detail_quantity', 'bill_detail.unit_price as bill_detail_unit_price',
                                    'customer.name as customer_name', 'customer.address as customer_address', 'products.name as products_name')
                                    ->where('bill_detail.confirm', 2)->get();
        //dd($bill_details);
        $bill = Bill::all();
        //dd($bill_details);
        
        return view('admin/donhang', compact('bill_details', 'bill_details_proccess', 'bill_details_success'));
    }

    public function postTiepNhanDonHang(Request $request)
    {
        $bill_detail = BillDetail::find($request->id_bill_detail_confirm);
        $bill_detail->confirm = 1;
        $bill_detail->save();

        return redirect('admin/donhang')->with('thongbao', 'Tiếp nhận đơn hàng thành công!');
    }

    public function postXoaDonHangChuaXacNhan(Request $request)
    {
        $bill_detail = BillDetail::find($request->id_bill_detail_delete_confirm)->delete();

        return redirect('admin/donhang')->with('thongbao', 'Xóa đơn đơn hàng thành công!');
    }

    public function postXuLyDonHang(Request $request)
    {
        $bill_detail = BillDetail::find($request->id_bill_detail_success);
        $bill_detail->confirm = 2;
        $bill_detail->save();

        return redirect('admin/donhang')->with('thongbao', 'Đánh dấu xử lý đơn hàng thành công!');
    }

    public function postXoaDonHangDaXacNhan(Request $request)
    {
        $bill_detail = BillDetail::find($request->id_bill_detail_delete_success)->delete();

        return redirect('admin/donhang')->with('thongbao', 'Xóa đơn đơn hàng thành công!');
    }

    public function getKhachHang()
    {
        $customers = Customer::all();
        return view('admin/khachhang', compact('customers'));
    }

    //Khach hang

    public function postThemKhachHang(Request $request)
    {
        $customer = new Customer();
        $customer->name = $request->tenkhachhang;
        $customer->gender = $request->gioitinh;
        $customer->email = $request->email;
        $customer->address = $request->diachi;
        $customer->phone_number = $request->sodienthoai;
        $customer->save();

        return redirect()->back()->with('thongbao', 'Thêm khách hàng thành công!');
    }

    public function postSuaKhachHang(Request $request)
    {
        $customer = Customer::find($request->idkhachhangsua);
        $customer->name = $request->tenkhachhangsua;
        $customer->gender = $request->gioitinhsua;
        $customer->email = $request->emailsua;
        $customer->address = $request->diachisua;
        $customer->phone_number = $request->sodienthoaisua;
        $customer->save();

        return redirect()->back()->with('thongbao', 'Sửa thông tin khách hàng thành công!');
    }

    public function postXoaKhachHang(Request $request)
    {
        $customer = Customer::find($request->idkhachhang)->delete();

        return redirect()->back()->with('thongbao', 'Đã xóa khách hàng!');
    }

    //Thanh vien

    public function getThanhVien()
    {
        $users = User::all();
        return view('admin/thanhvien', compact('users'));
    }

    public function postThemThanhVien(Request $request)
    {
        $this->validate($request, [
            'tenthanhvien' => 'required|max:255',
            'gioitinh' => 'required|in:1,0',
            'email' => 'required|email|unique:users,email',
            'diachi' => 'required|max:255',
            'sodienthoai' => 'max:13|required|min:10|digits_between:10,13',
            'matkhau' => 'min:6|max:25'
        ],
        [
            'tenthanhvien.required' => 'Tên khách hàng không được để trống.',
            'tenthanhvien.max' => 'Tên khách hàng không vượt quá 255 ký tự.',
            'gioitinh.required' => 'Giới tính không được để trống.',
            'email.required' => 'Địa chỉ email không được để trống.',
            'email.email' => 'Địa chỉ email không đúng định dạng.',
            'email.unique' => 'Địa chỉ email này đã được sử dụng.',
            'diachi.required' => 'Địa chỉ nhận hàng không được để trống.',
            'diachi.max' => 'Địa chỉ dài tối đa 255 ký tự',
            'sodienthoai.required' => 'Số điện thoại không được để trống.',
            'sodienthoai.digits_between' => 'Số điện thoại không đúng định dạng.',
            'sodienthoai.max' => 'Số điện thoại không quá 13 ký tự.',
            'sodienthoai.min' => 'Số điện thoại tối thiểu 10 ký tự',
            'matkhau.min' => 'Mật khẩu tối thiểu 6 ký tự',
            'matkhau.max' => 'Mật khẩu tối đã 25 ký tự'
        ]);

        $user = new User();
        $user->name = $request->tenthanhvien;
        $user->gender = $request->gioitinh;
        $user->email = $request->email;
        $user->address = $request->diachi;
        $user->phone = $request->sodienthoai;
        $user->password = Hash::make($request->matkhau);
        $user->role = $request->quyenhan;
        $user->save();

        return redirect()->back()->with('thongbao', 'Thêm thành viên thành công!');
    }

    public function postSuaThanhVien(Request $request)
    {
        $this->validate($request, [
            'tenthanhviensua' => 'required|max:255',
            'gioitinhsua' => 'required|in:1,0',
            'emailsua' => 'required|email',
            'diachisua' => 'required|max:255',
            'sodienthoaisua' => 'max:13|required|min:10|digits_between:10,13'
        ],
        [
            'tenthanhviensua.required' => 'Tên khách hàng không được để trống.',
            'tenthanhviensua.max' => 'Tên khách hàng không vượt quá 255 ký tự.',
            'gioitinhsua.required' => 'Giới tính không được để trống.',
            'emailsua.required' => 'Địa chỉ email không được để trống.',
            'emailsua.email' => 'Địa chỉ email không đúng định dạng.',
            'diachisua.required' => 'Địa chỉ nhận hàng không được để trống.',
            'diachisua.max' => 'Địa chỉ dài tối đa 255 ký tự',
            'sodienthoaisua.required' => 'Số điện thoại không được để trống.',
            'sodienthoaisua.digits_between' => 'Số điện thoại không đúng định dạng.',
            'sodienthoaisua.max' => 'Số điện thoại không quá 13 ký tự.',
            'sodienthoaisua.min' => 'Số điện thoại tối thiểu 10 ký tự'
        ]);

        $user = User::find($request->idthanhviensua);
        $user->name = $request->tenthanhviensua;
        $user->gender = $request->gioitinhsua;
        $user->email = $request->emailsua;
        $user->address = $request->diachisua;
        $user->phone = $request->sodienthoaisua;
        if(!empty($request->matkhausua))
            $user->password = Hash::make($request->matkhausua);
        $user->role = $request->quyenhansua;
        $user->save();

        return redirect()->back()->with('thongbao', 'Sửa thông tin thành viên thành công!');
    }

    public function postXoaThanhVien(Request $request)
    {
        $user = User::find($request->idthanhvien)->delete();

        return redirect()->back()->with('thongbao', 'Đã xóa thành viên khỏi hệ thống!');
    }

    public function getThongKe()
    {
        $now = Carbon::now();
        $sub = Carbon::now()->subYear(10);
        $year_now = $now->year;
        $year_sub = $sub->year;
        $year_arr = [];
        $year_push = array_push($year_arr, $year_now);
        $temp = -1;
        for($i=$year_sub; $i<=$year_now; $i++)
        {
            array_push($year_arr, $year_now + $temp);
            $temp--;
        }

        $bill_arr = [];
        for($i=1; $i<=12; $i++)
        {
            $bill = Bill::whereMonth('created_at', $i)->whereYear('created_at', $year_now)->sum('total_payment');
            array_push($bill_arr, $bill);
        }

        return view('admin/thongke', compact('year_arr', 'bill_arr', 'year_now'));
    }

    public function postThongKe(Request $request)
    {
        $namthongke = $request->namthongke;

        $now = Carbon::now();
        $sub = Carbon::now()->subYear(10);
        $year_now = $now->year;
        $year_sub = $sub->year;
        $year_arr = [];
        $year_push = array_push($year_arr, $year_now);
        $temp = -1;
        for($i=$year_sub; $i<=$year_now; $i++)
        {
            array_push($year_arr, $year_now + $temp);
            $temp--;
        }

        $bill_arr = [];
        for($i=1; $i<=12; $i++)
        {
            $bill = Bill::whereMonth('created_at', $i)->whereYear('created_at', $namthongke)->sum('total_payment');
            array_push($bill_arr, $bill);
        }
        
        $year_now = $namthongke;

        return view('admin/thongke', compact('year_arr', 'bill_arr', 'year_now'));
    }
}
