<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'trangchu',
	'uses' => 'pageController@getIndex'
]);

Route::get('index', [
	'as' => 'trangchu',
	'uses' => 'pageController@getIndex'
]);

Route::get('chi-tiet/{id}', [
	'as' => 'chitiet',
	'uses' => 'pageController@getChitiet'
]);

Route::get('loai-sp/{type}', [
	'as' => 'loaisp',
	'uses' => 'pageController@getLoaiSP'
]);

Route::get('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@getThanhtoan'
]);

Route::post('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@postThanhToan'
]);

Route::get('tim-kiem', [
	'as' => 'timkiem',
	'uses' => 'pageController@getTimKiem'
]);

Route::get('dang-ky', [
	'as' => 'dangky',
	'uses' => 'pageController@getDangKy'
]);

Route::post('dang-ky', [
	'as' => 'dangky',
	'uses' => 'pageController@postDangKy'
]);

Route::get('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'pageController@getDangNhap'
]);

Route::post('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'pageController@postDangNhap'
]);

Route::get('dang-xuat', [
	'as' => 'dangxuat',
	'uses' => 'PageController@getDangXuat'
]);


Route::get('gioi-thieu', [
	'as' => 'gioithieu',
	'uses' => 'pageController@getDangNhap'
]);

Route::get('cartAdd/{id}', [
	'as' => 'cartAdd',
	'uses' => 'pageController@getThemSanPham'
]);

Route::get('cartRemoveOne/{id}', [
	'as' => 'cartRemoveOne',
	'uses' => 'pageController@getXoaMotSanPham'
]);

Route::get('cartRemoveAll/{id}', [
	'as' => 'cartRemoveAll',
	'uses' => 'pageController@getXoaTatCaSP'
]);

/*Route::get('slide', [
	'as' => 'slide',
	'uses' => 'pageController@getSlide'
]);*/

Route::post('binh-luan', [
	'as' => 'postBinhLuan',
	'uses' => 'pageController@postBinhLuan'
]);

//admin route

Route::prefix('admin')->group(function () {

	Route::get('login', [
		'as' => 'admin/login',
		'uses' => 'AdminController@getLoginAdmin'
	]);

	Route::post('login', [
		'as' => 'admin/login',
		'uses' => 'AdminController@postLoginAdmin'
	]);

	Route::get('dangxuat', [
		'as' => 'admin/dangxuatadmin',
		'uses' => 'AdminController@getDangXuatAdmin'
	]);
	

	Route::get('trangchu', [
		'as' => 'admin/trangchu',
		'uses' => 'AdminController@getIndex'
	]);
	
	Route::get('sanpham', [
		'as' => 'admin/sanpham',
		'uses' => 'AdminController@getSanPham'
	]);

	Route::post('suasanphamAPI', [
		'as' => 'admin/suasanphamAPI',
		'uses' => 'AdminController@requestSuaSanPham'
	]);

	Route::post('postThemSanPham', [
		'as' => 'admin/postThemSanPham',
		'uses' => 'AdminController@postThemSanPham'
	]);

	Route::post('postSuaSanPham', [
		'as' => 'admin/postSuaSanPham',
		'uses' => 'AdminController@postSuaSanPham'
	]);

	Route::post('postXoaSanPham', [
		'as' => 'admin/postXoaSanPham',
		'uses' => 'AdminController@postXoaSanPham'
	]);

	Route::get('getSetNoiBat/id={id}/top={top}', [
		'as' => 'admin/getSetNoiBat',
		'uses' => 'AdminController@getSetNoiBat'
	]);

	Route::get('donhang', [
		'as' => 'admin/donhang',
		'uses' => 'AdminController@getDonHang'
	]);

	Route::post('tiepnhandonhang', [
		'as' => 'admin/tiepnhandonhang',
		'uses' => 'AdminController@postTiepNhanDonHang'
	]);

	Route::post('postXoaDonHangChuaXacNhan', [
		'as' => 'admin/postXoaDonHangChuaXacNhan',
		'uses' => 'AdminController@postXoaDonHangChuaXacNhan'
	]);
	
	Route::post('xulydonhang', [
		'as' => 'admin/xulydonhang',
		'uses' => 'AdminController@postXuLyDonHang'
	]);

	Route::post('postXoaDonHangDaXacNhan', [
		'as' => 'admin/postXoaDonHangDaXacNhan',
		'uses' => 'AdminController@postXoaDonHangDaXacNhan'
	]);

	Route::get('getKhachHang', [
		'as' => 'admin/getKhachHang',
		'uses' => 'AdminController@getKhachHang'
	]);

	Route::post('themKhachHang', [
		'as' => 'admin/themKhachHang',
		'uses' => 'AdminController@postThemKhachHang'
	]);

	Route::post('suaKhachHang', [
		'as' => 'admin/suaKhachHang',
		'uses' => 'AdminController@postSuaKhachHang'
	]);

	Route::post('xoaKhachHang', [
		'as' => 'admin/xoaKhachHang',
		'uses' => 'AdminController@postXoaKhachHang'
	]);

	Route::post('themThanhVien', [
		'as' => 'admin/themThanhVien',
		'uses' => 'AdminController@postThemThanhVien'
	]);

	Route::post('suaThanhVien', [
		'as' => 'admin/suaThanhVien',
		'uses' => 'AdminController@postSuaThanhVien'
	]);

	Route::post('xoaThanhVien', [
		'as' => 'admin/xoaThanhVien',
		'uses' => 'AdminController@postXoaThanhVien'
	]);

	Route::get('getThanhVien', [
		'as' => 'admin/getThanhVien',
		'uses' => 'AdminController@getThanhVien'
	]);

	Route::get('thong-ke', [
		'as' => 'admin/getThongKe',
		'uses' => 'AdminController@getThongKe'
	]);

	Route::post('thong-ke', [
		'as' => 'admin/postThongKe',
		'uses' => 'AdminController@postThongKe'
	]);
	
});