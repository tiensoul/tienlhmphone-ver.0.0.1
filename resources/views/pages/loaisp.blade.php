@extends('master')
@section('library')
<link rel="stylesheet" href="source/css/style.css">
@endsection('library')
@section('content')
<section class=" ">
   <h1 class="h1">Loại sản phẩm:  {{ $product_type_name->name }}</h1>
   <!-- Product -->
   <ul class="homeproduct">
      @foreach($product_type as $pt)
      <li>
         <a href="{{ route('chitiet', $pt->id) }}">
            <img width="180" height="180" class="lazyloaded" src="{{ $pt->image }}">
            <h3>{{ $pt->name }}</h3>
            <div class="price">
               @if($pt->promotion_price != 0)
                  <strong>{{ number_format(($pt->promotion_price), 0, ',', '.') }}₫</strong>
                  <span>{{ number_format(($pt->unit_price), 0, ',', '.') }}₫</span>
               @else
                  <strong>{{ number_format(($pt->unit_price), 0, ',', '.') }}₫</strong>
               @endif
            </div>
            <div class="promo noimage">
               <p>{{ $pt->description }}</p>
            </div>
            @if($pt->promotion_price != 0)
            <label class="installment-2">-{{ number_format((100 - ($pt->promotion_price / $pt->unit_price) * 100), 0) }}%</label>
            @endif    
         </a>
      </li>
      @endforeach
   </ul>
</section>
<!-- Tag  -->
<div class="loadingcover" style="display: none;">
   <p class="csslder">
      <span class="csswrap">
      <span class="cssdot"></span>
      <span class="cssdot"></span>
      <span class="cssdot"></span>
      </span>
   </p>
</div>
@endsection('content')