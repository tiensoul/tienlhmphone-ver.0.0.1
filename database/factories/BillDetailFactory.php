<?php

use Faker\Generator as Faker;

$factory->define(App\BillDetail::class, function (Faker $faker) {
    return [
        'id_product' => rand(1,10),
        'quantity' => rand(1,10),
        'unit_price' => rand(1000,100000000)
    ];
});
