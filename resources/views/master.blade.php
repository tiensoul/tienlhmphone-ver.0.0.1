<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>TMobile - Siêu thị điện thoại, Tablet, Laptop, Phụ kiện chính hãng</title>
      <base href="{{ asset('') }}">
      <meta name="keywords" content="Thế giới di động, TMobile, điện thoại di động, ViPhone, smartphone, tablet, máy tính bảng, Laptop, máy tính xách tay, phụ kiện điện thoại, tin công nghệ">
      <meta name="description" content="Hệ thống bán lẻ điện thoại di động, smartphone, máy tính bảng, tablet, laptop, phụ kiện chính hãng mới nhất, giá tốt, dịch vụ khách hàng được yêu thích nhất VN">
      <meta property="og:title" content="TMobile - Siêu thị điện thoại, Tablet, Laptop, Phụ kiện chính hãng">
      <meta property="og:description" content="Hệ thống bán lẻ điện thoại di động, smartphone, máy tính bảng, tablet, laptop, phụ kiện chính hãng mới nhất, giá tốt, dịch vụ khách hàng được yêu thích nhất VN">
      <link rel="stylesheet" href="source/css/font-awesome.min.css">
      @yield('library')
   </head>
   <body>
      @yield('content')
      @include('header')
      @include('footer')
      <p id="back-top">
         <a href="#" title="Về Đầu Trang"><span></span></a>
      </p>
   </body>
</html>