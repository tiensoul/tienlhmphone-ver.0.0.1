<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>TMobile - Đăng nhập quản trị</title>
	<base href="{{ asset('sourceadmin/login') }}">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Karla'>

	<link rel="stylesheet" href="login/css/style.css">


</head>

<body>

	<body>
		<div id="back">

			<div class="backRight"
				style="background-image: url('https://s14.postimg.cc/odfuw6ff5/login-right2.jpg'); background-size: cover; background-position: right;">
			</div>
			<div class="backLeft"
				style="background-image: url('https://images.unsplash.com/photo-1461773518188-b3e86f98242f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'); background-size: cover; background-position: left;">
			</div>
		</div>

		<div id="slideBox">
			<div class="topLayer">
				<div class="left" style="overflow: hidden">
					<div class="content">
						<img src="https://s14.postimg.cc/572nsqr4x/logo-single.png" style="
    width: 90px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
" />
						<form id="form-signup" method="post" onsubmit="return false;">
							<div class="form-element form-stack">
								<label for="email" class="form-label">Email</label>
								<input id="email" type="email" name="email">
							</div>
							<div class="form-element form-stack">
								<label for="username-signup" class="form-label">Username</label>
								<input id="username-signup" type="text" name="username">
							</div>
							<div class="form-element form-stack">
								<label for="password-signup" class="form-label">Password</label>
								<input id="password-signup" type="password" name="password">
							</div>
							<div class="form-element form-stack">
								<label for="password-signup" class="form-label">Konfirmasi Password</label>
								<input id="password-signup2" type="password" name="password2">
							</div>
							<div class="form-element form-submit">
								{{--  <button id="signUp" class="login" type="submit" name="signup">Daftar</button>  --}}
								<button id="goLeft" class="login off">Đăng nhập</button>
							</div>
						</form>
					</div>
				</div>
				<div class="right" style="overflow: hidden">
					<div class="content">
						<!--<h2>Masuk</h2>-->
						{{--  <img src="https://s14.postimg.cc/572nsqr4x/logo-single.png" style="
    width: 90px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
" />  --}}
						<form id="form-login" method="post" action="{{ route('admin/login') }}">
							@csrf
							<div class="form-element form-stack">
								<label for="username-login" class="form-label">Email</label>
								<input id="username-login" type="text" name="email">
							</div>
							<div class="form-element form-stack">
								<label for="password-login" class="form-label">Password</label>
								<input id="password-login" type="password" name="password">
							</div>
							<div class="form-element form-submit">
								<button id="logIn" class="login" type="submit" name="login">Login</button>
								{{--  <button id="goRight" class="login off" name="signup">Daftar</button>  --}}
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js'></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/paper.js/0.11.3/paper-full.min.js'></script>



	<script src="login/js/index.js"></script>




</body>

</html>