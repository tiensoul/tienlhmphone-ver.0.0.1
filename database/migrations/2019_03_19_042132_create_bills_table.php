<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_customer')->unsigned();
            $table->timestamp('date_order');
            $table->double('total')->comment('tổng tiền');
            $table->double('total_promotion')->comment('tổng tiền khuyến mãi');
            $table->double('total_payment')->comment('tổng tiền cần thanh toán');
            $table->string('payment')->comment('hình thức thanh toán');
            $table->text('note')->nullable();
            $table->timestamps();
            $table->foreign('id_customer')->references('id')->on('customer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
