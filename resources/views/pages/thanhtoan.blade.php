@extends('master')
@section('library')
<link rel="stylesheet" href="source/css/style.css">
@endsection('library')
@section('content')
<section id="wrap_cart">
   <div class="bar-top">
      <a href="{{ route('trangchu') }}" class="buymore">Mua thêm sản phẩm khác</a>
      <div class="yourcart">Giỏ hàng của bạn</div>
   </div>
   <div class="wrap_cart">
      @if(Session::has('cart'))
      <form action="{{ route('thanhtoan') }}" method="POST" id="checkout">
         @csrf
         <div class="detail_cart">
            <ul class="listorder">
               <li class="justadded">
                  @foreach($products as $p)
                  <div class="colimg">
                     <a href="{{ route('chitiet', $p['item']['id'])}}">
                     <img width="55" alt="{{ $p['item']['name'] }}" src="{{ $p['item']['image'] }}">
                     </a>
                  <button type="button" class="delete"><span></span><a href="{{ route('cartRemoveAll', $p['item']['id']) }}" style="color: #a5a4a4;">Xóa</a></button>
                  </div>
                  <div class="colinfo">
                     <strong>{{ number_format(($p['item']['unit_price']), 0, ',', '.') }}₫</strong>
                  <a href="{{ route('chitiet', $p['item']['id'])}}">{{ $p['item']['name'] }}</a>
                     <div class="promotion  webnote">
                        @if($p['item']['promotion_price'] != 0)
                        <span>
                           Giảm giá {{ number_format((100 - ($p['item']['promotion_price'] / $p['item']['unit_price']) * 100), 0) }}%
                        </span>
                        @else
                           &nbsp
                        @endif
                        <span>
                           <label class="infoend">Đang giảm giá</label>
                        </span>
                     </div>
                     <div>
                     @if($p['item']['promotion_price'] != 0)
                        <span>Giảm <strong style="float: none; margin-right: 0;">{{ number_format((($p['item']['unit_price'] - $p['item']['promotion_price'])), 0, ',', '.') }}₫</strong> còn <strong style="float: none;">{{ number_format(($p['item']['promotion_price']), 0, ',', '.')  }}₫</strong></span>
                     @else
                        &nbsp
                     @endif
                     <span></span>
                     </div>
                     <div class="choosenumber">
                        {{-- <div class="abate active"></div> --}}
                        <a class="@if($p['qty'] == 1) abate @else abate active @endif" href="{{route('cartRemoveOne', $p['item']['id'])}}"></a>
                        <div class="number">{{ $p['qty'] }}</div>
                        <a class="augment" href="{{route('cartAdd', $p['item']['id'])}}"></a>
                     </div>
                     <div class="clr"></div>
                  </div>
                  @endforeach
               </li>
            </ul>
            <div class="area_total">
               <div>
                  <div>
                     <span>Tổng tiền:</span>
                     <span>{{ number_format(($totalPrice), 0, ',', '.') }}₫</span>
                  </div>
                  <div>
                     @if($totalPromotionPrice != 0)
                        <span>Giảm:</span>
                        <span>{{ number_format(($totalPromotionPrice), 0, ',', '.') }}₫</span>
                     @else
                        <span>Giảm:</span>
                        <span>- 0₫</span>
                     @endif
                  </div>
                  <div class="shipping_store">
                     <div class="total">
                        <b>Cần thanh toán:</b>
                        @if($totalPromotionPrice == 0)
                           <strong>{{ number_format(($totalPrice - $totalPromotionPrice), 0, ',', '.') }}₫</strong>
                        @else
                           <strong>{{ number_format(($totalPrice - $totalPromotionPrice), 0, ',', '.') }}₫</strong>
                        @endif
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="infouser ">
            <div class="malefemale">
               <div class="alert-info">
                  Vui lòng điền điền đầy đủ thông tin dưới đây. Các trường có dấu <span style="color:red;">(*)</span> là bắt buộc
               </div>
            </div>
            <div class="areainfo">
               <div class="left">
               <input type="text" class="saveinfo @if($errors->has('name')) error @endif" name="name" placeholder="Họ và tên (*)" maxlength="255" value="@if(Auth::check()) {{ Auth::user()->name }} @elseif(old('name')) {{ old('name') }} @endif">
                  @if ($errors->has('name'))
                     <label class="error">{{$errors->first('name')}}</label>
                  @endif
               </div>
               <div class="right">
                  <input type="tel" class="saveinfo @if($errors->has('phone')) error @endif" name="phone" placeholder="Số điện thoại (*)" maxlength="11" value="@if(Auth::check()) {{ Auth::user()->phone }} @elseif(old('phone')) {{ old('phone') }} @endif">
                  @if ($errors->has('phone'))
                     <label class="error">{{$errors->first('phone')}}</label>
                  @endif
               </div>
               <select class="gender-custom @if($errors->has('gender')) error @endif" name="gender">
                  <option value="">--Giới tính--</option>
                  <option value="1" @if(Auth::check()) {{ Auth::user()->gender == 0 ? ' ' : 'selected' }} @else {{(old('gender') == '1') ? 'selected' : ''}} @endif>Nam</option>
                  <option value="0" @if(Auth::check()) {{ Auth::user()->gender == 0 ? 'selected' : ' ' }} @else {{(old('gender') == '0') ? 'selected' : ''}} @endif>Nữ</option>
               </select>
               @if ($errors->has('gender'))
                  <label class="error">{{$errors->first('gender')}}</label>
               @endif
               <input type="email" name="email" placeholder="Địa chỉ Email (*)" maxlength="100" class="saveinfo @if($errors->has('email')) error @endif" value="@if(Auth::check()) {{ Auth::user()->email }} @elseif(old('email')) {{ old('email') }} @endif">
               @if ($errors->has('email'))
                  <label class="error">{{$errors->first('email')}}</label>
               @endif
               <input type="text" class="saveinfo @if($errors->has('address')) error @endif" name="address" placeholder="Địa chỉ nhận hàng (*)" maxlength="400" value="@if(Auth::check()) {{ Auth::user()->address }} @elseif(old('address')) {{ old('address') }} @endif">
               @if ($errors->has('address'))
                  <label class="error">{{$errors->first('address')}}</label>
               @endif
               <input type="text" class="saveinfo" name="note" placeholder="Ghi chú" maxlength="400" value="{{ old('note') }}">
            </div>
            </div>
         <div class="choosepayment">
            <button type="submit" class="payoffline" form="checkout" name="off">Thanh toán khi nhận hàng<span>Xem hàng trước, không mua không sao</span></button>
            <button type="submit" class="payonline" form="checkout" name="on">Thanh toán trực tuyến<span>Bằng thẻ ATM, VISA, MASTER</span></button>
         </div>
         <div class="clr"></div>
         
         
         <div class="link-thanhtoan" style="text-align: center;margin-bottom: 20px;">
            <a href="#" target="_blank" style="color: #288ad6;">
            Chính sách hoàn tiền khi thanh toán online
            </a>
         </div>
         <div class="clr"></div>
      </form>
      @endif
      @if(Session::has('thongbao'))
      <div class="detail_cart">
            <ul class="listorder">
            </ul>
            <div class="area_total">
               <div class="alert-success">
                     <strong>Thành công!</strong> Đơn hàng của bạn đã được tiếp nhận, vui lòng kiểm tra email để xem chi tiết đơn hàng.
               </div>
            </div>
         </div>
      @endif
   </div>
   <br><br><br>
</section>
@endsection('content')