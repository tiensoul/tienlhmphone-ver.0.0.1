@extends('master')
@section('library')
<link rel="stylesheet" href="source/css/style.css">
@endsection('library')

@section('content')
<div style="margin-top: 100px;"></div>
<section id="wrap_cart">
   <div class="bar-top">
      <a href={{ URL::previous() }} class="buymore">Trở lại trang trước</a>
      <div class="yourcart">Khu vực khách hàng</div>
   </div>
   <div class="wrap_cart">
   <form action="{{ route('dangnhap') }}" method="post">
      @csrf
         <div class="infouser ">
            <div class="total"><br>
               @if(!Session::has('message'))
               <div class="alert-info">
                     Vui lòng đăng nhập để bình luận, xem thông tin đơn hàng và khám phá nhiều hơn
               </div>
               @endif

               @if(Session::has('message'))
                  <div class="alert-{{ Session::get('flag') }}">
                     <strong>Thông báo!</strong> {{ Session::get('message') }}
                  </div>
               @endif
            </div>
            <div class="areainfo-custom">
               <p>Địa chỉ Email / Số điện thoại <span style="color:red;">(*)</span></p>
               <input type="text" class="saveinfo @if($errors->has('email')) error @endif" name="email" placeholder="Email / SĐT" maxlength="100" value="{{ old('email') }}">
                  @if ($errors->has('email'))
                     <label class="error">{{$errors->first('email')}}</label>
                  @endif

               <p>Mật khẩu <span style="color:red;">(*)</span></p>
               <input type="password" class="saveinfo @if($errors->has('password')) error @endif" name="password" placeholder="Mật khẩu" maxlength="13" value="{{ old('phone') }}">
               @if ($errors->has('password'))
                  <label class="error">{{$errors->first('password')}}</label>
               @endif
            </div>
         </div>
         <div class="choosepayment">
            <input type="submit" class="button button-blue" value="Đăng nhập"></input>
            </a>
         </div>
         <div class="clr"></div>
      </form>
   </div>
</section>
<div style="margin-bottom: 300px;"></div>
@endsection('content')