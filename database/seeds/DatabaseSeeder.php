<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
        	CustomerSeeder::class,
            UserSeeder::class,
            // ProductTypeSeeder::class
        	//BillsSeeder::class
            //BillDetailSeeder::class
            ProductTypeSeeder::class,
            ProductSeeder::class,
            SlideSeeder::class
            //ProductSeeder::class
        ]);
    }
}
