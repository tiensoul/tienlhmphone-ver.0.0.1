<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Factory(App\User::class, 15)->create();
        
        DB::table('users')->insert([
            ['name' => 'Lê Minh Tiến', 'phone' => '0964979247', 'gender' => '1', 'email' => 'tienlee.ask@gmail.com', 'address' => 'Minh Khai, Hà Nội', 'role' => 1, 'password' => '$2y$10$wKryRucMQtQWIUHq8O.PxeOEtRrIIZnliJpnQcg8Fu6iu903Z.snm'],
            ['name' => 'Lê Thị Thủy', 'phone' => '0988888888', 'gender' => '0', 'email' => 'thuyle@gmail.com', 'address' => 'Hai Bà Trưng, Hà Nội', 'role' => 0, 'password' => '$2y$10$wKryRucMQtQWIUHq8O.PxeOEtRrIIZnliJpnQcg8Fu6iu903Z.snm']
        ]);
    }
}
