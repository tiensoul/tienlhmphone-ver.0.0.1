@extends('master')
@section('library')
<link rel="stylesheet" href="source/css/style.css">
@endsection('library')
@section('content')
<section>
   <!-- start homebanner-->
   <aside class="homebanner">
      <div id="sync1" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
         <div id="slides">
            @foreach($slide as $sl)
            <img src="{{ $sl->image }}" alt="{{ $sl->link }}">
            @endforeach
            <!-- <a href="#" class="slidesjs-previous slidesjs-navigation"></a>
            <a href="#" class="slidesjs-next slidesjs-navigation"></a> -->
         </div>
         <br>
         <div id="sync2" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
               <div class="owl-wrapper-outer">
                  <div class="owl-wrapper" style="width: 1896px; left: 0px; display: block; transition: all 200ms ease 0s; transform: translate3d(0px, 0px, 0px);">
                     @foreach($productType as $pt)
                     <div class="owl-item" style="width: 158px;">
                        <div class="item">
                           <h3>{{ $pt->name }}</h3>
                           <i class="arrowbar"></i>
                        </div>
                     </div>
                     @endforeach
                  </div>
               </div>
            </div>
      </div>
   </aside>
   <!-- end homebanner-->
   <!--start homenews-->
   <aside class="homenews">
      <div class="twobanner">
         @foreach($slide as $sl)
         <a href="#"><img src="{{ $sl->banner }}" alt="#"></a>
         @endforeach
      </div>
   </aside>
   <!--end homenews-->
   <div class="clr"></div>
   <!--start khuyen mai hot nhat-->
   <div class="">
      <div class="navigat ">
         <h2>KHUYẾN MÃI HOT NHẤT</h2>
      </div>
      <div id="owl-promo" class="owl-carousel homepromo  owl-theme" style="opacity: 1; display: block;">
         <div class="owl-wrapper-outer">
            <div class="owl-wrapper" style="width: 6240px; left: 0px; display: block;">
               @foreach($product as $p)
               <div class="owl-item" style="width: 240px;">
                  <div class="item">
                     <!--#region Ngành hàng chính -->
                     <a href="{{ route('chitiet', $p->id) }}">
                        <img width="180" height="180" src="{{ $p->image }}" alt="">
                        <h3>{{ $p->name }}</h3>
                        <div class="price">
                           @if($p->promotion_price != 0)
                              <strong>{{ number_format(($p->promotion_price), 0, ',', '.') }}₫</strong>
                              <span>{{ number_format(($p->unit_price), 0, ',', '.') }}₫</span>
                           @else
                              <strong>{{ number_format(($p->unit_price), 0, ',', '.') }}₫</strong>
                           @endif
                        </div>
                        @if($p->promotion_price != 0)
                        <label class="shockprice">Giảm {{ number_format((100 - ($p->promotion_price / $p->unit_price) * 100), 0) }}%</label>
                        @endif
                        <div class="promo noimage">
                           <p>{{ $p->description }}</p>
                        </div>
                     </a>
                     <!--#endregion -->
                  </div>
               </div>
               @endforeach
            </div>
         </div>
      </div>
      <!--end khuyen mai hot nhat-->
      <!--class navigat start-->
      <div class="navigat">
         <h2>Điện thoại nổi bật nhất</h2>
      </div>
      <!--class navigat end-->
      <!-- home products -->
      <ul class="homeproduct ">
         <li class="feature">
            @foreach($slide->take(1) as $sl)
            <a href="#">
               <img width="600" height="275" class="lazyloaded" id="noTrans" alt="#" src="{{ $sl->top_image }}">
               <h3 id="mar">Samsung Galaxy Note 9 512GB</h3>
               <div class="price">
                  <strong>24.490.000₫</strong>
                  <span>28.490.000₫</span>
               </div>
               <label class="new">GIẢM 4.000.000₫</label>
            </a>
            @endforeach
         </li>
         @foreach($product_top as $pt)
         <li>
            <!--#region Ngành hàng chính -->
            <a href="{{ route('chitiet', $pt->id) }}">
               <img width="180" height="180" class="lazyloaded" alt="#" src="{{ $pt->image }}">
               <h3>{{ $pt->name }}</h3>
               <div class="price">
                  @if($pt->promotion_price != 0)
                     <strong>{{ number_format(($pt->promotion_price), 0, ',', '.') }}₫</strong>
                     <span>{{ number_format(($pt->unit_price), 0, ',', '.') }}₫</span>
                  @else
                     <strong>{{ number_format(($pt->unit_price), 0, ',', '.') }}₫</strong>
                  @endif
               </div>
               <div class="promo noimage">
                  <p>{{ $pt->description }}</p>
               </div>
               @if($pt->promotion_price != 0)
               <label class="installment">-{{ number_format((100 - ($pt->promotion_price / $pt->unit_price) * 100), 0) }}%</label>
               @endif
            </a>
            <!--#endregion -->
         </li>
         @endforeach
         <li class="feature">
            @foreach($slide->take(1) as $sl)
            <a href="#">
               <img width="600" height="275" class="lazyloaded" id="noTrans" alt="#" src="{{ $sl->top_image }}">
               <h3 id="mar">Samsung Galaxy Note 9 512GB</h3>
               <div class="price">
                  <strong>24.490.000₫</strong>
                  <span>28.490.000₫</span>
               </div>
               <label class="shockprice">GIẢM 4.000.000₫</label>
            </a>
            @endforeach
         </li>
         @foreach($product_top_line_2 as $pt)
         <li>
            <!--#region Ngành hàng chính -->
            <a href="{{ route('chitiet', $pt->id) }}">
               <img width="180" height="180" class=" lazyloaded" alt="#" src="{{ $pt->image }}">
               <h3>{{ $pt->name }}</h3>
               <div class="price">
                  @if($pt->promotion_price != 0)
                     <strong>{{ number_format(($pt->promotion_price), 0, ',', '.') }}₫</strong>
                     <span>{{ number_format(($pt->unit_price), 0, ',', '.') }}₫</span>
                  @else
                     <strong>{{ number_format(($pt->unit_price), 0, ',', '.') }}₫</strong>
                  @endif
               </div>
               <div class="promo noimage">
                  <p>{{ $pt->description }}</p>
               </div>
               @if($pt->promotion_price != 0)
               <label class="installment-2">-{{ number_format((100 - ($pt->promotion_price / $pt->unit_price) * 100), 0) }}%</label>
               @endif
            </a>
            <!--#endregion -->
         </li>
         @endforeach
      </ul>
      <!-- end home products -->

      <div class="navigat">
         <h2>Tất cả sản phẩm</h2>
      </div>
      <ul class="homeproduct ">
         @foreach($product_random as $pr)
         <li>
            <!--#region Ngành hàng chính -->
            <a href="{{ route('chitiet', $pr->id) }}">
               <img width="180" height="180" class=" lazyloaded" alt="#" src="{{ $pr->image }}">
               <h3>{{ $pr->name}}</h3>
               <div class="price">
                  @if($pr->promotion_price != 0)
                     <strong>{{ number_format(($pr->promotion_price), 0, ',', '.') }}₫</strong>
                     <span>{{ number_format(($pr->unit_price), 0, ',', '.') }}₫</span>
                  @else
                     <strong>{{ number_format(($pr->unit_price), 0, ',', '.') }}₫</strong>
                  @endif
               </div>
               <div class="promo noimage">
                  <p>{{ $p->description }}</p>
               </div>
               @if($pr->promotion_price != 0)
            <label class="new">Đang giảm giá -{{ number_format((100 - ($pr->promotion_price / $pr->unit_price) * 100), 0) }}%</label>
               @endif
            </a>
            <!--#endregion -->
         </li>
         @endforeach
      </ul>
      {{ $product_random->links() }}

      <!--#endregion -->
   </div>
   </div></div></div>
   </br>
   </div>
</section>
   <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
   <!-- End SlidesJS Required -->

   <!-- SlidesJS Required: Link to jquery.slides.js -->
  <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/139719/jquery.slides.min.js"></script>
  <script>
    $(function() {
      $('#slides').slidesjs({
        width: 940,
        height: 325,
        navigation: false,
        play: { auto: true,interval: 6000, },
        effect: {
         slide: {speed: 1500},
         fade: { speed: 400, },
            }
         });
      });
  </script>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src='https://s3-us-west-2.amazonaws.com/s.cdpn.io/139719/jquery.slides.min.js'></script>
@endsection('content')