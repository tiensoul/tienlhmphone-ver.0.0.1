@extends('admin/master')
@section('content')
@if(!Auth::check())
  <script>window.location = "{{ route('admin/login') }}";</script>
@endif

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Quản lý đơn hàng
        <small>Quản lý tiếp nhận và xử lý đơn hàng</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
        <li><a href="#">Quản lý đơn hàng</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách đơn hàng chưa xử lý</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="donhangchuaxuly" class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Tên khách hàng</th>
                    <th>Địa chỉ nhận hàng</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Tổng tiền</th>
                    <th>Khuyễn mãi</th>
                    <th>Cần thanh toán</th>
                    <th>Ngày đặt</th>
                    <th>Ghi chú</th>
                    <th>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bill_details as $bd)
                    <tr>
                        <td>{{ $bd->bill_detail_id }}</td>
                        <td>{{ $bd->customer_name }}</td>
                        <td>{{ $bd->customer_address }}</td>
                        <td><a href="{{ route('chitiet', $bd->bill_detail_id_product) }}" target="_blank">{{ $bd->products_name }}</a></td>
                        <td>{{ $bd->bill_detail_quantity }}</td>
                        <td>{{ number_format(($bd->bills_total), 0, ',', '.') }}</td>
                        <td>{{ number_format(($bd->bills_total_promotion), 0, ',', '.')}}</td>
                        <td>{{ number_format(($bd->bills_total_payment), 0, ',', '.') }}</td>
                        <td>{{ $bd->bills_date_order }}</td>
                        <td>{{ $bd->bills_payment }}</td>
                        {{--  <td><a href="{{ route('admin/getSetNoiBat', [$gap->id_product, $gap->top_product]) }}"><i class="fa @if($gap->top_product == 1) fa-star star-active @else fa-star-o star-none  @endif"></i></a></td>
                        <td>{{ $gap->short_description_product }}</td>
                        <td>@if($gap->promotion_price == 0) {{ number_format(($gap->unit_price), 0, '.', ',') }} @else {{ number_format(($gap->promotion_price), 0, '.', ',') }} @endif</td>  --}}
                    <td>
                        <a class="btn btn-xs bg-green dt-confirm" href="#" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-check-square"></i></a>
                        <a class="btn btn-xs bg-red dt-delete-confirm" href="#" data-toggle="modal" data-target="#modal-delete-confirm"><i class="fa fa-trash-o"></i></a>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách đơn hàng đang xử lý</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="donhangchuaxuly" class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Tên khách hàng</th>
                    <th>Địa chỉ nhận hàng</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Tổng tiền</th>
                    <th>Khuyễn mãi</th>
                    <th>Cần thanh toán</th>
                    <th>Ngày đặt</th>
                    <th>Ghi chú</th>
                    <th>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bill_details_proccess as $bdp)
                    <tr>
                        <td>{{ $bdp->bill_detail_id }}</td>
                        <td>{{ $bdp->customer_name }}</td>
                        <td>{{ $bdp->customer_address }}</td>
                        <td><a href="{{ route('chitiet', $bdp->bill_detail_id_product) }}" target="_blank">{{ $bdp->products_name }}</a></td>
                        <td>{{ $bdp->bill_detail_quantity }}</td>
                        <td>{{ number_format(($bdp->bills_total), 0, ',', '.') }}</td>
                        <td>{{ number_format(($bdp->bills_total_promotion), 0, ',', '.')}}</td>
                        <td>{{ number_format(($bdp->bills_total_payment), 0, ',', '.') }}</td>
                        <td>{{ $bdp->bills_date_order }}</td>
                        <td>{{ $bdp->bills_payment }}</td>
                        {{--  <td><a href="{{ route('admin/getSetNoiBat', [$gap->id_product, $gap->top_product]) }}"><i class="fa @if($gap->top_product == 1) fa-star star-active @else fa-star-o star-none  @endif"></i></a></td>
                        <td>{{ $gap->short_description_product }}</td>
                        <td>@if($gap->promotion_price == 0) {{ number_format(($gap->unit_price), 0, '.', ',') }} @else {{ number_format(($gap->promotion_price), 0, '.', ',') }} @endif</td>  --}}
                    <td>
                        <a class="btn btn-xs bg-green dt-success" href="#" data-toggle="modal" data-target="#modal-success"><i class="fa fa-spinner"></i></a>
                        <a class="btn btn-xs bg-red dt-delete-success" href="#" data-toggle="modal" data-target="#modal-delete-success"><i class="fa fa-trash-o"></i></a>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách đơn hàng đã xử lý</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="donhangchuaxuly" class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Tên khách hàng</th>
                    <th>Địa chỉ nhận hàng</th>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Tổng tiền</th>
                    <th>Khuyễn mãi</th>
                    <th>Cần thanh toán</th>
                    <th>Ngày đặt</th>
                    <th>Ghi chú</th>
                    <th>Trạng thái</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bill_details_success as $bds)
                    <tr>
                        <td>{{ $bds->bill_detail_id }}</td>
                        <td>{{ $bds->customer_name }}</td>
                        <td>{{ $bds->customer_address }}</td>
                        <td><a href="{{ route('chitiet', $bds->bill_detail_id_product) }}" target="_blank">{{ $bds->products_name }}</a></td>
                        <td>{{ $bds->bill_detail_quantity }}</td>
                        <td>{{ number_format(($bds->bills_total), 0, ',', '.') }}</td>
                        <td>{{ number_format(($bds->bills_total_promotion), 0, ',', '.')}}</td>
                        <td>{{ number_format(($bds->bills_total_payment), 0, ',', '.') }}</td>
                        <td>{{ $bds->bills_date_order }}</td>
                        <td>{{ $bds->bills_payment }}</td>
                        {{--  <td><a href="{{ route('admin/getSetNoiBat', [$gap->id_product, $gap->top_product]) }}"><i class="fa @if($gap->top_product == 1) fa-star star-active @else fa-star-o star-none  @endif"></i></a></td>
                        <td>{{ $gap->short_description_product }}</td>
                        <td>@if($gap->promotion_price == 0) {{ number_format(($gap->unit_price), 0, '.', ',') }} @else {{ number_format(($gap->promotion_price), 0, '.', ',') }} @endif</td>  --}}
                    <td>
                        <i class="fa fa-check-square"></i>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
<!-- Add the sidebars background. This div must be placed immediately after the control sidebar -->

<!--- Modal -->
<form action="{{ route('admin/tiepnhandonhang') }}" method="post">
@csrf
<div class="modal fade" id="modal-confirm" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thông báo</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_bill_detail_confirm" name="id_bill_detail_confirm">
        <label for="exampleInputEmail1">Bạn có muốn đánh dấu đang xử lý đơn hàng này?</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-warning">Tiếp nhận đơn hàng</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!--- Modal -->
<form action="{{ route('admin/xulydonhang') }}" method="post">
@csrf
<div class="modal fade" id="modal-success" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thông báo</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_bill_detail_success" name="id_bill_detail_success">
        <label for="exampleInputEmail1">Bạn có muốn đánh dấu đã xử lý hoàn tất đơn hàng này?</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-success">Đánh dấu đã xử lý</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!--- Modal -->
<form action="{{ route('admin/postXoaDonHangChuaXacNhan') }}" method="post">
@csrf
<div class="modal fade" id="modal-delete-confirm" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thông báo</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_bill_detail_delete_confirm" name="id_bill_detail_delete_confirm">
        <label for="exampleInputEmail1">Trước khi hủy đơn hàng nhân viên vui lòng thông báo trước tới khách hàng!</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-danger">Xóa đơn hàng</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!--- Modal -->
<form action="{{ route('admin/postXoaDonHangDaXacNhan') }}" method="post">
@csrf
<div class="modal fade" id="modal-delete-success" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thông báo</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="id_bill_detail_delete_success" name="id_bill_detail_delete_success">
        <label for="exampleInputEmail1">Trước khi hủy đơn hàng nhân viên vui lòng thông báo trước tới khách hàng!</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-danger">Xóa đơn hàng</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!-- page script -->
<script>
  $(function () {
    $('#donhangchuaxuly').DataTable({
      'paging': true,
      'lengthChange': false,
      'searching': false,
      'ordering': true,
      'info': true,
      'autoWidth': false,
      "searching": true,
      "ordering": false
    });
  });
</script>
<!-- END SCRIPT -->

<script>
    $('.dt-confirm').each(function () {
        $(this).on('click', function(evt){
            $this = $(this);
            var dtRow = $this.parents('tr');
            for(var i=0; i < dtRow[0].cells.length; i++){
            //console.log(dtRow[0].cells[i].innerHTML);
            $('#id_bill_detail_confirm').val(dtRow[0].cells[0].innerHTML);
            }
        });
    });

    $('.dt-delete-confirm').each(function () {
        $(this).on('click', function(evt){
            $this = $(this);
            var dtRow = $this.parents('tr');
            for(var i=0; i < dtRow[0].cells.length; i++){
            //console.log(dtRow[0].cells[i].innerHTML);
            $('#id_bill_detail_delete_confirm').val(dtRow[0].cells[0].innerHTML);
            }
        });
    });

    $('.dt-success').each(function () {
        $(this).on('click', function(evt){
            $this = $(this);
            var dtRow = $this.parents('tr');
            for(var i=0; i < dtRow[0].cells.length; i++){
            //console.log(dtRow[0].cells[i].innerHTML);
            $('#id_bill_detail_success').val(dtRow[0].cells[0].innerHTML);
            }
        });
    });

    $('.dt-delete-success').each(function () {
        $(this).on('click', function(evt){
            $this = $(this);
            var dtRow = $this.parents('tr');
            for(var i=0; i < dtRow[0].cells.length; i++){
            //console.log(dtRow[0].cells[i].innerHTML);
            $('#id_bill_detail_delete_success').val(dtRow[0].cells[0].innerHTML);
            }
        });
    });
</script>
@endsection('content')