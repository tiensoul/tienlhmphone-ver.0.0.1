@extends('master')
@section('library')
<link rel="stylesheet" href="source/css/style.css">
@endsection('library')
@section('content')
<body id="bg-all">
<section class="type0" id="bg-all">
   <ul class="breadcrumb">
      <li><a class="home" href="{{ route('trangchu') }}" title="Trang chủ">Trang chủ</a><span>›</span></li>
      <li><a href="{{ route('loaisp', $product_type->id) }}" title="#">{{ $product_type->name }}</a></li>
   </ul>
   <div class="rowtop">
      <h1>Sản phẩm: {{ $product->name }}</h1>
   </div>
   <div class="clr"></div>
   <div class="rowdetail" id="normalproduct">
      <aside class="picture">
         <div class="border-frame">
            <img src="{{ $product->image }}" alt="">
         </div>
         <div class="colorandpic ftAcc">
            <ul class="owl-carousel owl-theme tabscolor" style="opacity: 1; display: block;">
               <div class="owl-wrapper-outer">
                  <div class="owl-wrapper" style="width: 1064px; left: 76px; display: block;">
                     <div class="owl-item" style="width: 76px;">
                        <li class="item">
                           <div>
                              <img src="{{ $product->image }}">
                           </div>
                        </li>
                     </div>
                     <div class="owl-item" style="width: 76px;">
                        <li class="item">
                           <div>
                              <img src="{{ $product->image }}">
                           </div>
                        </li>
                     </div>
                     <div class="owl-item" style="width: 76px;">
                        <li class="item">
                           <div>
                              <img src="{{ $product->image }}">
                           </div>
                        </li>
                     </div>
                     <div class="owl-item" style="width: 76px;">
                        <li class="item">
                           <div>
                              <img src="{{ $product->image }}">
                           </div>
                        </li>
                     </div>
                  </div>
               </div>
            </ul>
         </div>
      </aside>
      <aside class="price_sale">
         <div class="area_price">
            @if($product->promotion_price != 0)
               <strong>{{ number_format(($product->promotion_price), 0, ',', '.') }}₫</strong>
               <span class="hisprice">{{ number_format(($product->unit_price), 0, ',', '.') }}₫</span>
            @else
               <strong class="hisprice">{{ number_format(($product->unit_price), 0, ',', '.') }}₫</strong>
            @endif
            <span></span>
         </div>
         <div class="notechoose"></div>
         <div class="area_order">
         <a href="{{ route('cartAdd', ['id' => $product->id]) }}" class="buy_now">Mua ngay<span>Giao tận nơi hoặc nhận ở cửa hàng</span></a>
         </div>
         <div class="callorder">
            <div class="ct">
               <span>Gọi đặt mua: <a href="tel:0964979247">1800.xxxx</a> (miễn phí)</span>
            </div>
         </div>
         <ul class="plcAcc">
            <li style="text-align: justify;">{{ $product->short_description }}</li>
         </ul>
      </aside>
      <div class="rightInfo">
         <ul class="policy ">
            <li class="wrpr ">
               Bảo hành 2 tuần 1 đổi 1 (nếu lỗi).
               <a href="#">Xem chi tiết</a>
            </li>
            <li class="shpr">
               <span>Phí giao hàng <strong>10.000đ</strong> (Áp dụng đơn hàng dưới <strong class="f">200.000đ</strong>)</span>
            </li>
         </ul>
      </div>
   </div>
   <div class="clr"></div>
   <div class="box_content">
      <aside class="left_content">
         <div class="clr"></div>
         {!! $product->long_description !!}
         <div class="clr"></div>
         <div class="comdetail">
            <div class="wrap_comment" title="Bình luận sản phẩm" id="comment">
               <div class="tltCmt">
                  <form action="{{ route('postBinhLuan') }}" method="POST">
                     @csrf
                     <input type="hidden" name="id_product" value="{{ $product->id }}">
                     <textarea id="txtEditor" name="noidungbinhluan" cols="" rows="" class="parent_input txtEditor" placeholder="Mời bạn để lại bình luận..." required></textarea>
                     @if(count($errors) > 0)
                        <p style="color: red; padding: 2px 0;">{{ $errors->first() }}</p>
                     @endif

                     @if(Session::has('thongbao'))
                        <p style="color: #3512ea; padding: 2px 0;">{{ Session::get('thongbao') }}</p>
                     @endif
                     <input class="binhluan" type="submit" value="Gửi bình luận">
                  </form>
                  <div class="clr"></div>
                  <div class="midcmt">
                     <span class="totalcomment">{{ count($comments) }} bình luận        </span>        
                  </div>
               </div>
               <ul class="listcomment">
                  @foreach($comments as $comment)
                  <li class="comment_ask">
                     <div class="rowuser">
                        <a href="javascript:void(0)">
                           <div>{{ mb_substr($comment->user->name, 0, 1, 'utf-8') }}</div>
                           <strong>{{ $comment->user->name }}</strong>
                        </a>
                     </div>
                     <div class="question">{{ $comment->content }}</div>
                     <div class="inputreply hide"></div>
                  </li>
                  @endforeach
               </ul>
            </div>
         </div>
      </aside>
      <aside class="right_content">
         <div class="accessories">
            <div>
               <h3>Các sản phẩm khác</h3>
            </div>
            <ul>
               @foreach($product_random as $pr)
               <li>
                  <a href="{{ route('chitiet', $pr->id) }}">
                     <img class="lazy" src="{{ $pr->image }}" style="display: block;">
                     <h3>{{ $pr->name }}</h3>
                     @if($pr->promotion_price != 0)
                        <strong>{{ number_format(($pr->promotion_price), 0, ',', '.') }}₫</strong>
                        <span class="hisprice" style="text-decoration: line-through;">{{ number_format(($pr->unit_price), 0, ',', '.') }}đ</span>
                     @else
                        <strong>{{ number_format(($pr->unit_price), 0, ',', '.') }}₫</strong>
                     @endif
                  </a>
               </li>
               @endforeach
            </ul>
         </div>
      </aside>
   </div>
   <div class="clr test01"></div>
</section>
</body>
@endsection('content')