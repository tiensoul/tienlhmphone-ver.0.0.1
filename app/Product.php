<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name', 'id_type', 'short_description', 'long_description', 'unit_price', 'promotion_price', 'image', 'top'
    ];


    public function bill_detail()
    {
    	return $this->hasMany('App\BillDetail', 'id_product', 'id');
    }

    public function type_products()
    {
    	return $this->belongsTo('App\ProductType', 'id_type', 'id');
    }
}
