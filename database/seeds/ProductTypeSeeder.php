<?php

use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Factory(App\ProductType::class, 7)
        // 	->create()
        // 	->each(function($pt){
        // 		$pt->products()->save(factory(App\Product::class)->make());
        // 	});

        DB::table('type_products')->insert([
            ['name' => 'Iphone', 'description' => 'No description', 'image' => 'No image'],
            ['name' => 'Samsung', 'description' => 'No description', 'image' => 'No image'],
            ['name' => 'Huawei', 'description' => 'No description', 'image' => 'No image'],
            ['name' => 'Xiaomi', 'description' => 'No description', 'image' => 'No image'],
            ['name' => 'Oppo', 'description' => 'No description', 'image' => 'No image'],
            ['name' => 'Asus', 'description' => 'No description', 'image' => 'No image'],
            ['name' => 'HTC', 'description' => 'No description', 'image' => 'No image']
            // ['name' => 'Nokia', 'description' => 'No description', 'image' => 'No image']
        ]);

    }
}
