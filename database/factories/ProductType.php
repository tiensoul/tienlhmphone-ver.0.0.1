<?php

use Faker\Generator as Faker;

$factory->define(App\ProductType::class, function (Faker $faker) {
    return [
        'name' => $faker->colorName,
        'description' => $faker->paragraph($nbSentences = 8),
        'image' => $faker->imageUrl($width = 400, $height = 400, 'technics')
    ];
});
