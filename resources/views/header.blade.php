<header>
   <div class="wrap-main">
      <a class="logo" title="TMobile" href="{{ route('trangchu') }}">
      <i class="hadoan-logo"><img src="https://i.imgur.com/GS2xfo4.png" alt="TMobile"></i>
      </a>
      <form id="search-site" action="{{ route('timkiem') }}" method="get" autocomplete="off">
         <input class="topinput" id="search-keyword" name="keyword" type="text" aria-label="Bạn tìm gì..." placeholder="Bạn cần tìm gì..." autocomplete="off" maxlength="50">
         <button class="btntop" type="submit" aria-label="tìm kiếm"><i class="icontgdd-topsearch"></i></button>
      </form>
      <nav>
         @foreach($loaisp as $lsp)
         <a href="{{ route('loaisp', $lsp->id) }}" class="mobile" title="Điện thoại di động, smartphone">
            {{ $lsp->name }}
         </a>
         @endforeach
         @if(Auth::check())
         <a href="{{ route('trangchu') }}" style="width: 100px;">
            {{ Auth::user()->name }}
         </a>
         <a href="{{ route('dangxuat') }}" style="width: 100px;">
               Đăng xuất
            </a>
         @else
         <a href="{{ route('dangky') }}">
            Đăng ký
         </a>
         <a href="{{ route('dangnhap') }}">
            Đăng nhập
         </a>
         @endif
         <a href="{{ route('thanhtoan') }}" class="ask badge1" title="" @if(Session::has('cart')) data-badge="{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}" @endif>
         <i class="hadoan-ask"></i>Giỏ hàng</a>
      </nav>
   </div>
   <div class="clr"></div>
</header>