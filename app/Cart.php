<?php

namespace App;

class Cart
{
	public $items = null;
	public $totalQty = 0;
	public $totalPrice = 0;
	public $totalPromotionPrice = 0;

	public function __construct($oldCart){
		if($oldCart){
			$this->items = $oldCart->items;
			$this->totalQty = $oldCart->totalQty;
			$this->totalPrice = $oldCart->totalPrice;
			$this->totalPromotionPrice = $oldCart->totalPromotionPrice;
		}
	}

	public function add($item, $id){
		$giohang = ['qty'=>0, 'price' => $item->unit_price, 'promotion_price' => $item->promotion_price, 'item' => $item];
		if($this->items){
			if(array_key_exists($id, $this->items)){
				$giohang = $this->items[$id];
			}
		}
		$giohang['qty']++;
		$giohang['price'] = $item->unit_price * $giohang['qty'];
		if($item->promotion_price == 0)
			$giohang['promotion_price'] = 0;
		else
			$giohang['promotion_price'] = ($item->unit_price * $giohang['qty']) - ($item->promotion_price * $giohang['qty']);
			//$giohang['promotion_price'] .= $this->totalPromotionPrice;
		$this->items[$id] = $giohang;
		$this->totalQty++;
		if($this->totalPromotionPrice == 0)
			$this->totalPromotionPrice += $giohang['promotion_price'];
		elseif($this->totalPromotionPrice != 0 && $item->promotion_price != 0 && $giohang['qty'] != 1)
			$this->totalPromotionPrice = $giohang['promotion_price'];
		elseif($this->totalPromotionPrice != 0 && $item->promotion_price != 0 && $giohang['qty'] == 1)
			$this->totalPromotionPrice = $this->totalPromotionPrice + ($item->unit_price * $giohang['qty']) - ($item->promotion_price * $giohang['qty']);
		elseif($item->promotion_price == 0)
			$this->totalPromotionPrice = $this->totalPromotionPrice;
		else
			$this->totalPromotionPrice = 0;
		$this->totalPrice += $item->unit_price;
	}

	//xóa 1
	public function reduceByOne($id){
		$this->items[$id]['qty']--;
		$this->items[$id]['price'] -= $this->items[$id]['item']['unit_price'];
		$this->totalQty--;
		$this->totalPrice -= $this->items[$id]['item']['unit_price'];
		if($this->items[$id]['item']['promotion_price'] == 0)
			$this->items[$id]['promotion_price'];
		else
			$this->items[$id]['promotion_price'] = $this->items[$id]['item']['unit_price'] * $this->items[$id]['qty'] - $this->items[$id]['item']['promotion_price'] * $this->items[$id]['qty'];
		if($this->items[$id]['qty'] < 1)
			$this->totalPromotionPrice -= $this->totalPromotionPrice; //$this->items[$id]['item']['promotion_price'];
		elseif($this->totalPromotionPrice != 0 &&  $this->items[$id]['item']['promotion_price']!= 0 && $this->items[$id]['qty'] != 1 || $this->items[$id]['qty'] == 1) {
			//$this->totalPromotionPrice = $this->totalPromotionPrice - $this->items[$id]['item']['unit_price'] - $this->items[$id]['item']['promotion_price'] != 0 ? $this->items[$id]['item']['promotion_price'] : 0;
			//$this->totalPromotionPrice = $this->totalPrice - ($this->totalPrice - $this->items[$id]['item']['promotion_price'] != 0 ? $this->items[$id]['item']['promotion_price'] * $this->items[$id]['qty'] : $this->totalPrice);
			$this->totalPromotionPrice -= ($this->items[$id]['promotion_price'] / $this->items[$id]['qty']);
		}
		elseif($this->totalPromotionPrice != 0 &&  $this->items[$id]['item']['promotion_price']!= 0 && $this->items[$id]['qty'] == 1)
			$this->totalPromotionPrice += $this->totalPromotionPrice - ($this->items[$id]['item']['unit_price'] - $this->items[$id]['item']['promotion_price']);
		else
			$this->totalPromotionPrice += $this->items[$id]['item']['unit_price'] - $this->items[$id]['item']['promotion_price'] != 0 ? $this->items[$id]['item']['promotion_price'] :  $this->items[$id]['item']['unit_price'];
		if($this->items[$id]['qty']<=0){
			unset($this->items[$id]);
		}
	}
	//xóa nhiều
	public function removeItem($id){
		$this->totalQty -= $this->items[$id]['qty'];
		$this->totalPrice -= $this->items[$id]['price'];
		$this->totalPromotionPrice -= $this->items[$id]['promotion_price'];
		unset($this->items[$id]);
	}
}
