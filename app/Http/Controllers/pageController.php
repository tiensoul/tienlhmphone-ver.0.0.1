<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\ProductType;
use App\Product;
use App\Cart;
use App\Customer;
use App\Bill;
use App\BillDetail;
use App\User;
use App\Comment;
use Auth;
use Hash;
use Session;
use Carbon\Carbon;


class pageController extends Controller
{
    public function getIndex()
    {
        $slide = Slide::all()->take(5);
        $productType = ProductType::all()->take(5);

        $product = Product::all();

        $product_random = Product::inRandomOrder()->paginate(10);

        $product_top = Product::where('top', 1)->inRandomOrder()->limit(3)->get();

        $product_top_line_2 = Product::where('top', 1)->inRandomOrder()->limit(3)->get();
        //dd($product_top);

    	return view('pages.trangchu', compact('slide', 'productType','product', 'product_random', 'product_top', 'product_top_line_2'));
    }

    public function getChitiet($id)
    {
        // $to = Carbon::createFromFormat('Y-m-d H:s:i', '2015-5-6 3:30:34');
        // $from = Carbon::createFromFormat('Y-m-d H:s:i', '2015-5-6 3:30:54');


        // $diff_in_minutes = $to->diffInMinutes($from);
        // dd($diff_in_minutes); // Output: 20

        $product = Product::find($id);
        $product_type = $product->type_products;
        $product_random = Product::all()->random(4);

        $comments = Comment::where('id_product', $id)->with('user')->get();

    	return view('pages.chitiet', compact('product', 'product_type' ,'product_random', 'comments'));
    }

    public function getLoaiSP($type)
    {
        $product_type = Product::where('id_type', $type)->get();
        $product_type_name = ProductType::where('id', $type)->first();
    	return view('pages.loaisp', compact('product_type', 'product_type_name'));
    }

    public function getThemSanPham(Request $request, $id)
    {
        $product = Product::find($id);
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldcart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);
        // dd($request->session()->get('cart'));

    	return redirect()->back();
    }

    public function getXoaMotSanPham(Request $request, $id)
    {
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldcart);
        $cart->reduceByOne($id);

        Session::put('cart', $cart);
        // dd($request->session()->get('cart'));

    	return redirect()->back();
    }

    public function getXoaTatCaSP($id)
    {
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldcart);
        $cart->removeItem($id);

        Session::put('cart', $cart);

        return redirect()->back();
    }

    public function getThanhtoan(Request $request)
    {
        if(!Session::has('cart')) {
            return view('pages.thanhtoan', ['products' => null]);
        }
        $oldcart = Session::get('cart');
        $cart = new Cart($oldcart);
        //dd($cart->items, $cart->totalPrice,  $cart->totalPromotionPrice);
        return view('pages.thanhtoan', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'totalPromotionPrice' => $cart->totalPromotionPrice]);
    }

    public function postThanhToan(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'gender' => 'required|in:1,0',
            'email' => 'required|email',
            'address' => 'required|max:255|min:10',
            'phone' => 'max:13|required|min:10|digits_between:10,13'
        ],
        [
            'name.required' => 'Tên khách hàng không được để trống.',
            'name.max' => 'Tên khách hàng không vượt quá 255 ký tự.',
            'gender.required' => 'Giới tính không được để trống.',
            'email.required' => 'Địa chỉ email không được để trống.',
            'email.email' => 'Địa chỉ email không đúng định dạng.',
            'address.required' => 'Địa chỉ nhận hàng không được để trống.',
            'address.max' => 'Địa chỉ tối đa 255 ký tự',
            'address.min' => 'Vui lòng nhập một địa chỉ hợp lệ',
            'phone.required' => 'Số điện thoại không được để trống.',
            'phone.digits_between' => 'Số điện thoại không đúng định dạng.',
            'phone.max' => 'Số điện thoại không quá 13 ký tự.',
            'phone.min' => 'Số điện thoại tối thiểu 10 ký tự'
        ]);
        $cart = Session::get('cart');
        //dd($cart);
        $customer = new Customer;
        $customer_find = Customer::where('email', $request->email)->first();
        $customer_find_count = Customer::where('email', $request->email)->count();
        // /dd($customer_find->id);
        if($customer_find_count > 0)
        {
            if($request->note == '')
                $note = "Không có ghi chú";
            else
                $note = $request->note;
            
            $customer_find = Customer::where('email', $request->email)
                ->update([
                    'name' => $request->name,
                    'gender' => $request->gender,
                    'email' => $request->email,
                    'address' => $request->address,
                    'phone_number' => $request->phone,
                    'note' => $note
                ]);
            // $customer_find->name = $request->name;
            // $customer_find->gender = $request->gender;
            // $customer_find->email = $request->email;
            // $customer_find->email = $request->address;
            // $customer_find->phone_number = $request->phone;
            // if($request->note == '')
            //     $customer_find->note = "Không có ghi chú";
            // else
            //     $customer_find->note = $request->note;
            // $customer_find->save();
        } else {
            //$customer = new Customer;
            $customer->name = $request->name;
            $customer->gender = $request->gender;
            $customer->email = $request->email;
            $customer->address = $request->address;
            $customer->phone_number = $request->phone;
            if($request->note == '')
                $customer->note = "Không có ghi chú";
            else
                $customer->note = $request->note;
            $customer->save();
        }

        $customer_find = Customer::where('email', $request->email)->first();
        $bill = new Bill;
        if($customer_find_count > 0)
        {
            //dd($customer_find->id);
            $bill->id_customer =  $customer_find->id;
        }
        else
            $bill->id_customer =  $customer->id;
        //dd($bill->id_customer);
        //$bill->date_order = date('Y-m-d');
        $bill->total = $cart->totalPrice;
        $bill->total_promotion = $cart->totalPromotionPrice;
        $bill->total_payment = ($cart->totalPrice - $cart->totalPromotionPrice);
        if(isset($_POST['off']))
            $payment = "COD";
        if(isset($_POST['on']))
            $payment = "CARD";
        $bill->payment = $payment;
        $bill->note = $request->note;
        $bill->save();
        

        foreach($cart->items as $key=>$value)
        {
            $billDetail = new BillDetail;
            $billDetail->id_bill = $bill->id;
            $billDetail->id_product = $key;
            $billDetail->quantity = $value['qty'];
            $billDetail->unit_price = ($value['price'] / $value['qty']);
            $billDetail->save();
        }
        Session::forget('cart');
        return redirect()->back()->with('thongbao', 'Đặt hàng thành công. Vui lòng kiểm tra email để xem chi tiết đơn hàng của bạn!');


    }

    public function getDangKy()
    {
    	return view('pages.dangky');
    }

    public function postDangKy(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'gender' => 'required|in:1,0',
            'email' => 'required|email|unique:users,email',
            'address' => 'required|max:255',
            'phone' => 'max:13|required|min:10|digits_between:10,13',
            'password' => 'min:6|max:25',
            're-password' => 'min:6|max:20|same:password'
        ],
        [
            'name.required' => 'Tên khách hàng không được để trống.',
            'name.max' => 'Tên khách hàng không vượt quá 255 ký tự.',
            'gender.required' => 'Giới tính không được để trống.',
            'email.required' => 'Địa chỉ email không được để trống.',
            'email.email' => 'Địa chỉ email không đúng định dạng.',
            'email.unique' => 'Địa chỉ email này đã được sử dụng.',
            'address.required' => 'Địa chỉ nhận hàng không được để trống.',
            'address.max' => 'Địa chỉ dài tối đa 255 ký tự',
            'phone.required' => 'Số điện thoại không được để trống.',
            'phone.digits_between' => 'Số điện thoại không đúng định dạng.',
            'phone.max' => 'Số điện thoại không quá 13 ký tự.',
            'phone.min' => 'Số điện thoại tối thiểu 10 ký tự',
            'password.min' => 'Mật khẩu tối thiểu 6 ký tự',
            'password.max' => 'Mật khẩu tối đã 25 ký tự',
            're-password.min' => 'Mật khẩu tối thiểu 6 ký tự',
            're-password.max' => 'Mật khẩu tối đã 20 ký tự',
            're-password.same' => 'Mật khẩu không giống nhau.',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->gender = $request->gender;
        $user->email = $request->email;
        $user->address = $request->address;
        $user->phone = $request->phone;
        $user->password = Hash::make($request->password);
        $user->save();

        //dd($user);
        return redirect()->back()->with('thongbao', 'bạn đã tạo tài khoản thành công trên hệ thống, bây giờ bạn có thể đăng nhập để mua sắm và bình luận trên bài viết!');
    }

    public function getTimKiem(Request $request)
    {
        $keyshow = $request->keyword;
        $loaisp = ProductType::all();
        $search_result = Product::where('name', 'like', '%'. $request->keyword . '%')->orWhere('unit_price', 'like', '%' . $request->keyword . '%')->paginate(6);
        //dd($search_result);
        return view('pages.timkiem', compact('search_result', 'loaisp', 'keyshow'));
    }


    public function getDangNhap()
    {
    	return view('pages.dangnhap');
    }

    public function getDangXuat()
    {
        Auth::logout();
        return redirect('/');
    }

    public function postDangNhap(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'password' => 'required|max:20'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Địa chỉ email tối đa 60 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu tối đa 20 kí tự'
        ]);

        $info = array('email' => $request->email, 'password' => $request->password);
        if(Auth::attempt($info)) {
            return redirect('/')->with(['flag' => 'success', 'message' => 'Đăng nhập thành công!']);
        } else {
            return redirect()->back()->with(['flag' => 'danger', 'message' => 'Đăng nhập không thành công vui lòng kiểm tra lại!']);
        }
    }

    public function postBinhLuan(Request $request)
    {
        if(!Auth::check()) {
            return redirect()->back()->withErrors('Vui lòng đăng nhập để đăng bình luận');
        }

        $comment = new Comment();
        $comment->content = $request->noidungbinhluan;
        $comment->id_product = $request->id_product;
        $comment->id_user = Auth::user()->id;
        $comment->status = 0;
        $comment->save();

        return redirect()->back()->with(['thongbao' => 'Bình luận của bạn đã được gửi đi và sẽ được hiển thị sau khi kiểm duyệt!']);

    }
}
