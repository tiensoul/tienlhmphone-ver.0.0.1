@extends('admin/master')
@section('content')
@if(!Auth::check())
  <script>window.location = "{{ route('admin/login') }}";</script>
@endif

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Quản lý sản phẩm
      <small>Quản lý thêm sửa xóa sản phẩm</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
      <li><a href="#">Quản lý sản phẩm</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Danh sách sản phẩm</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <button type="button" class="btn btn-success" data-target="#modal-them" data-toggle="modal">Thêm sản phẩm</button>
            <table id="sanpham" class="table table-bordered table-hover text-center">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Tên sản phẩm</th>
                  <th>Loại sản phẩm</th>
                  <th>Nổi bật</th>
                  <th>Mô tả ngắn</th>
                  <th>Giá sản phẩm</th>
                  <th>Thao tác</th>
                </tr>
              </thead>
              <tbody>
                @foreach($get_all_product as $gap)
                <tr>
                  <td>{{ $gap->id_product }}</td>
                  <td>{{ $gap->name_product }}</td>
                  <td>{{ $gap->name_type_product }}</td>
                  <td><a href="{{ route('admin/getSetNoiBat', [$gap->id_product, $gap->top_product]) }}"><i class="fa @if($gap->top_product == 1) fa-star star-active @else fa-star-o star-none  @endif"></i></a></td>
                  <td>{{ $gap->short_description_product }}</td>
                  <td>@if($gap->promotion_price == 0) {{ number_format(($gap->unit_price), 0, '.', ',') }} @else {{ number_format(($gap->promotion_price), 0, '.', ',') }} @endif</td>
                  <td>
                    <a class="btn btn-xs bg-blue dt-edit"><i class="fa fa-edit"></i></a>
                    <a class="btn btn-xs bg-red dt-delete" href="#" data-toggle="modal" data-target="#modal-xoa"><i class="fa fa-trash-o"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- Add the sidebars background. This div must be placed immediately after the control sidebar -->

<!--- Modal -->
<form action="{{ route('admin/postThemSanPham') }}" method="post" enctype='multipart/form-data'>
@csrf
<div class="modal fade" id="modal-them" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thêm sản phẩm mới</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Tên sản phẩm</label>
            <input type="text" class="form-control" id="tensanphamthem" name="tensanphamthem" placeholder="Tên sản phẩm" required {{  old('tensanphamthem') }}>
          </div>
          <div class="form-group">
            <label for="sel1">Loại sản phẩm</label>
            <select class="form-control" id="loaisanphamthem" name="loaisanphamthem" required>
              @foreach ($get_all_type_products as $gatp)
                <option value="{{ $gatp->id }}">{{ $gatp->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Mô tả ngắn gọn về sản phẩm</label>
            <input type="text" class="form-control" id="motasanphamthem" name="motasanphamthem" placeholder="Mô tả ngắn gọn" required>
          </div>
          <div class="form-group">
              <label for="">Thêm ảnh cho sản phẩm (ảnh đầu tiên sẽ được đặt làm ảnh bìa)</label>
              <div class="row">
                <div class="col-md-12">
                  <input type="file" name="hinhanhthem1" required>
                  <br>
                </div>
                <div class="col-md-12">
                  <input type="file" name="hinhanhthem2">
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Giá gốc của sản phẩm</label>
            <input type="text" class="form-control" id="giagocsanphamthem" name="giagocsanphamthem" placeholder="Giá gốc" required>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Giá khuyến mãi của sản phẩm (nếu có)</label>
            <input type="text" class="form-control" id="giakhuyenmaisanphamthem" name="giakhuyenmaisanphamthem" placeholder="Giá khuyến mãi">
          </div>
          <div class="form-group">
            <label for="sel1">Đặt làm sản phẩm nổi bật</label>
            <select class="form-control" id="sanphamnoibatthem" name="sanphamnoibatthem" required>
              <option value="1">Có</option>
              <option value="0">Không</option>
            </select>
          </div>
          <div class="form-group">
              <label for=""></label>
            <div class="row">
              <div class="col-md-12">
                <input type="file" name="hinhanhthem3">
                <br>
              </div>
              <div class="col-md-12">
                <input type="file" name="hinhanhthem4">
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
              <label for="sel1">Mô tả chi tiết về sản phẩm</label>
              <textarea id="editor2" rows="10" cols="80" name="motachitietthem" required></textarea>
          </div>
        </div>
        <div class="row"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-primary">Lưu lại</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!--- Modal -->
<form action="{{ route('admin/postSuaSanPham') }}" method="post" enctype='multipart/form-data'>
@csrf
<div class="modal fade" id="modal-sua" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Cập nhật sản phẩm</h4>
      </div>
      <div class="modal-body">
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Tên sản phẩm</label>
            <input type="hidden" class="form-control" id="idsanpham" name="idsanpham">
            <input type="text" class="form-control" id="tensanpham" name="tensanpham" placeholder="Tên sản phẩm">
          </div>
          <div class="form-group">
            <label for="sel1">Loại sản phẩm</label>
            <select class="form-control" id="loaisanpham" name="loaisanpham">
              @foreach ($get_all_type_products as $gatp)
                <option value="{{ $gatp->id }}">{{ $gatp->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Mô tả ngắn gọn về sản phẩm</label>
            <input type="text" class="form-control" id="motasanpham" name="motasanpham" placeholder="Mô tả ngắn gọn">
          </div>
          <div class="form-group">
              <label for="">Thêm ảnh cho sản phẩm (ảnh đầu tiên sẽ được đặt làm ảnh bìa)</label>
              <div class="row">
                <div class="col-md-12">
                  <input type="file" name="hinhanhsua1" required>
                  <br>
                </div>
                <div class="col-md-12">
                  <input type="file" name="hinhanhsua2">
                </div>
              </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Giá gốc của sản phẩm</label>
            <input type="text" class="form-control" id="giagocsanpham" name="giagocsanpham" placeholder="Giá gốc">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Giá khuyến mãi của sản phẩm (nếu có)</label>
            <input type="text" class="form-control" id="giakhuyenmaisanpham" name="giakhuyenmaisanpham" placeholder="Giá khuyến mãi">
          </div>
          <div class="form-group">
            <label for="sel1">Đặt làm sản phẩm nổi bật</label>
            <select class="form-control" id="sanphamnoibat" name="sanphamnoibat">
              <option value="1">Có</option>
              <option value="0">Không</option>
            </select>
          </div>
          <div class="form-group">
              <label for=""></label>
            <div class="row">
              <div class="col-md-12">
                <input type="file" name="hinhanhsua3">
                <br>
              </div>
              <div class="col-md-12">
                <input type="file" name="hinhanhsua4">
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
              <label for="">Ảnh bìa hiện tại</label>
              <span class="input-group-addon">
              <img src="#" alt="" id="hinhanh_hientai" width="100" height="100">
              </span>
            </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
              <label for="sel1">Mô tả chi tiết về sản phẩm</label>
              <textarea id="editor1" rows="10" cols="80" name="motachitiet"></textarea>
          </div>
        </div>
        <div class="row"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-primary">Lưu lại</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!--- Modal -->
<form action="{{ route('admin/postXoaSanPham') }}" method="post">
@csrf
<div class="modal fade" id="modal-xoa" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thông báo</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" id="idsanphamcanxoa" name="idsanphamcanxoa">
        <label for="exampleInputEmail1">Xác nhận xóa sản phẩm này?</label>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-danger">Xóa sản phẩm</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!-- SCRIPT -->
<script>
    $.ajaxSetup({
      beforeSend: function(){
        $('#modal-sua').css('display', 'none');
      }
   });
  </script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor2')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#sanpham').DataTable({
      'paging': true,
      'lengthChange': false,
      'searching': false,
      'ordering': false,
      'info': true,
      'autoWidth': false,
      "searching": true,
    })
  })
</script>
<!-- END SCRIPT -->
<script>
  //Edit row buttons
  $('.dt-edit').each(function () {
    $(this).on('click', function(evt){
      $this = $(this);
      var dtRow = $this.parents('tr');
      for(var i=0; i < dtRow[0].cells.length; i++){
        //console.log(dtRow[0].cells[i].innerHTML);
        $('#idsanpham').val(dtRow[0].cells[0].innerHTML);
        $('#idsanphamcanxoa').val(dtRow[0].cells[0].innerHTML);
        {{--  $('#tensanpham').val(dtRow[0].cells[1].innerHTML);
        $('#loaisanpham').val(dtRow[0].cells[1].innerHTML);
        $('#mota').val(dtRow[0].cells[4].innerHTML);
        $('#price').val(dtRow[0].cells[5].innerHTML);
        $('#promotion_price').val(dtRow[0].cells[6].innerHTML);  --}}
      }
    });
  });

  $('.dt-delete').each(function () {
    $(this).on('click', function(evt){
      $this = $(this);
      var dtRow = $this.parents('tr');
      for(var i=0; i < dtRow[0].cells.length; i++){
        //console.log(dtRow[0].cells[i].innerHTML);
        $('#idsanphamcanxoa').val(dtRow[0].cells[0].innerHTML);
      }
    });
  });

</script>

<!--edit product -->
<script>
$('.dt-edit').click(function(){
  $.ajax({
    url : "{{ route('admin/suasanphamAPI') }}",
    type : "post",
    dataType: "json",
    data : {
      _token : '{{ csrf_token() }}',
      idsanpham : $('#idsanpham').val()
    },
    success : function (result){
      console.log(result);
      $('#tensanpham').val(result['name']);
      $('#motasanpham').val(result['short_description']);
      $('#giagocsanpham').val(result['unit_price']);
      $('#sanphamnoibat').val(result['top']);
      $('#giakhuyenmaisanpham').val(result['promotion_price']);
      $('#loaisanpham').val(result['id_type']);
      
      $("#hinhanh_hientai").attr("src", '../../' + result['image']);
      //$('.cke_show_borders').val(result['long_description']);
      //CKEDITOR.setData(result['long_description']);
      //CKEDITOR.instances.mail_msg.insertText(obj["template"]);
      CKEDITOR.instances.editor1.setData(result['long_description'], {
        callback: function() {
            this.checkDirty(); // true
        }
    } );
      $("#modal-sua").modal();
      console.log(result);
      console.log(result['name']);
  }
});
  });
</script>
<!--end edit product-->
<script src="dist/js/bootstrap-notify.js"></script>
<script>
    @if(count($errors) > 0)
    @foreach($errors->all() as $err)
    $.notify('<strong>Lỗi:</strong> {{ $err }}',
    { allow_dismiss: false,
      placement:
      {
        from: "top",
        align: "right"
      },
      type: "danger",
      delay: 7000,
    });
    @endforeach
    @endif

    @if(Session::has('thongbao'))
      $.notify('<strong>Thành công:</strong> {{ Session::get('thongbao') }}',
      { allow_dismiss: false,
        placement:
        {
          from: "top",
          align: "right"
        },
        type: "info",
        delay: 7000,
      });
    @endif

</script>
@endsection('content')