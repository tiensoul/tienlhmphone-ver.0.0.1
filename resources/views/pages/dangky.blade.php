@extends('master')
@section('library')
<link rel="stylesheet" href="source/css/style.css">
@endsection('library')

@section('content')
<section id="wrap_cart">
   <div class="bar-top">
      <a href={{ URL::previous() }} class="buymore">Trở lại trang trước</a>
      <div class="yourcart">Tạo tài khoản khách hàng</div>
   </div>
   <div class="wrap_cart">
   <form action="{{ route('dangky') }}" method="post">
      @csrf
         <div class="infouser ">
            <div class="total"><br>
               @if(!Session::has('thongbao'))
               <div class="alert-info">
                     <strong>Thông báo!</strong> Vui lòng điền điền đầy đủ thông tin dưới đây. Các trường có dấu <span style="color:red;">(*)</span> là bắt buộc
               </div>
               @endif

               @if(Session::has('thongbao'))
                  <div class="alert-success">
                     <strong>Chúc mừng!</strong> {{ Session::get('thongbao') }}
                  </div>
               @endif
            </div>
            <div class="areainfo-custom">
               <p>Họ và tên <span style="color:red;">(*)</span></p>
               <input type="text" class="saveinfo @if($errors->has('name')) error @endif" name="name" placeholder="Vui lòng nhập họ và tên" maxlength="100" value="{{ old('name') }}">
                  @if ($errors->has('name'))
                     <label class="error">{{$errors->first('name')}}</label>
                  @endif

               <p>Số điện thoại <span style="color:red;">(*)</span></p>
               <input type="text" class="saveinfo @if($errors->has('phone')) error @endif" name="phone" placeholder="Số điện thoại" maxlength="13" value="{{ old('phone') }}">
               @if ($errors->has('phone'))
                  <label class="error">{{$errors->first('phone')}}</label>
               @endif

               <p>Giới tính <span style="color:red;">(*)</span></p>
               <select class="gender-custom @if($errors->has('gender')) error @endif" name="gender">
                  <option value="">--Giới tính--</option>
                  <option value="1" {{(old('gender') == '1') ? 'selected' : ''}}>Nam</option>
                  <option value="0" {{(old('gender') == '0') ? 'selected' : ''}}>Nữ</option>
               </select>
               @if ($errors->has('gender'))
                  <label class="error">{{$errors->first('gender')}}</label>
               @endif

               <p>Địa chỉ Email <span style="color:red;">(*)</span></p>
               <input type="text" name="email" placeholder="Vui lòng nhập địa chỉ Email" maxlength="100" value="{{ old('email') }}" class="saveinfo @if($errors->has('email')) error @endif">
               @if ($errors->has('email'))
                  <label class="error">{{$errors->first('email')}}</label>
               @endif

               <p>Địa chỉ nhận hàng</p>
               <input type="text" class="saveinfo" name="address" value="{{ old('address') }}" placeholder="Vui lòng nhập địa chỉ nhận hàng" maxlength="300">

               <p>Mật khẩu <span style="color:red;">(*)</span></p>
               <input type="password" class="saveinfo @if($errors->has('password')) error @endif" name="password" value="" placeholder="Mật khẩu" maxlength="300">
               @if ($errors->has('password'))
                  <label class="error">{{$errors->first('password')}}</label>
               @endif

               <p>Nhập lại mật khẩu <span style="color:red;">(*)</span></p>
               <input type="password" class="saveinfo @if($errors->has('re-password')) error @endif" name="re-password" value="" placeholder="Nhập lại mật khẩu" maxlength="300">
               @if ($errors->has('re-password'))
                  <label class="error">{{$errors->first('re-password')}}</label>
               @endif
            </div>
         </div>
         <div class="choosepayment">
            <div class="choosepayment"> <input type="submit" class="button button-green" value="Tạo tài khoản"></input> </div>
         </div>
         <div class="clr"></div>
      </form>
   </div>
</section>
<div style="margin-bottom: 100px;"></div>
@endsection('content')