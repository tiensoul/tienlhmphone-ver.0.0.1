@extends('admin/master')
@section('content')
@if(!Auth::check())
  <script>window.location = "{{ route('admin/login') }}";</script>
@endif

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Quản lý khách hàng
        <small>Quản lý thống kê báo cáo</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
        <li><a href="#">Quản lý báo cáo thống kê</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
                <h3 class="box-title">Thống kê</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-4">
                    <form action="{{ route('admin/postThongKe') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">Năm</label>
                                <select name="namthongke" id="namthongke" class="form-control">
                                    @foreach($year_arr as $year)
                                        <option value="{{ $year }}">{{ $year }}</option>
                                    @endforeach
                                </select>
                            </div>
                        <input type="submit" class="btn btn-success" value="Xem thống kê">
                    </form>
                </div>
                <div class="col-md-8">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="fa fa-bar-chart-o"></i>
              
                            <h3 class="box-title">Thống kê thu nhập năm {{ $year_now }}</h3>
                          </div>
                          <div class="box-body">
                            <div id="bar-chart" style="height: 300px;"></div>
                          </div>
                        <!-- /.box-body-->
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
<!-- /.content-wrapper -->
<!-- Add the sidebars background. This div must be placed immediately after the control sidebar -->

<!-- FLOT CHARTS -->
<script src="bower_components/Flot/jquery.flot.js"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="bower_components/Flot/jquery.flot.resize.js"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
<script src="bower_components/Flot/jquery.flot.pie.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="bower_components/Flot/jquery.flot.categories.js"></script>

<script>
    /*
     * BAR CHART
     * ---------
     */

     var bar_data = {
        data : [ 
            @foreach($bill_arr as $key => $b) ['Tháng {{ $key+1 }}', {{ $b }}], @if($key == 12) ['Tháng {{ $key }}', {{ $b }}] @endif @endforeach],
        color: '#3c8dbc'
      }
      $.plot('#bar-chart', [bar_data], {
        grid  : {
          borderWidth: 1,
          borderColor: '#f3f3f3',
          tickColor  : '#f3f3f3'
        },
        series: {
          bars: {
            show    : true,
            barWidth: 0.5,
            align   : 'center'
          }
        },
        xaxis : {
          mode      : 'categories',
          tickLength: 0
        }
      })
      /* END BAR CHART */
</script>

<script src="dist/js/bootstrap-notify.js"></script>

<script>
    @if(count($errors) > 0)
    @foreach($errors->all() as $err)
    $.notify('<strong>Lỗi:</strong> {{ $err }}',
    { allow_dismiss: false,
      placement:
      {
        from: "top",
        align: "right"
      },
      type: "danger",
      delay: 7000,
    });
    @endforeach
    @endif

    @if(Session::has('thongbao'))
      $.notify('<strong>Thành công:</strong> {{ Session::get('thongbao') }}',
      { allow_dismiss: false,
        placement:
        {
          from: "top",
          align: "right"
        },
        type: "info",
        delay: 7000,
      });
    @endif

</script>
@endsection('content')