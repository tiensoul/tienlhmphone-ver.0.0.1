<?php

use Faker\Generator as Faker;

$factory->define(App\Bill::class, function (Faker $faker) {
    return [
        'total' => rand(1000, 100000000),
        'total_promotion' => rand(1000, 100000000),
        'total_payment' => rand(1000, 100000000),
        'payment' => $faker->creditCardType,
        'note' => $faker->paragraph($nb = 3)
    ];
});
