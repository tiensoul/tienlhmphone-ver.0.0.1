@extends('admin/master')
@section('content')
@if(!Auth::check())
  <script>window.location = "{{ route('admin/login') }}";</script>
@endif

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Quản lý thành viên
        <small>Quản lý thêm sửa xóa thành viên trên hệ thống</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
        <li><a href="#">Quản lý thành viên</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header">
                <h3 class="box-title">Danh sách thành viên đã đăng ký trên hệ thống</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <button class="btn btn-info" data-toggle="modal" data-target="#themthanhvien">Thêm thành viên mới</button>
                <br>
                <table id="khachhang" class="table table-bordered table-hover text-center">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Tên thành viên</th>
                    <th>Địa chỉ Email</th>
                    <th>Giới tính</th>
                    <th>Quyền hạn</th>
                    <th>Số điện thoại</th>
                    <th>Địa chỉ</th>
                    <th>Thao tác</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $customer)
                    <tr>
                        <td>{{ $customer->id }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->email }}</td>
                        <td>@if($customer->gender == 1) Nam @else Nữ @endif</td>
                        <td>@if($customer->role == 1) <i class="fa fa-key" aria-hidden="true"></i> Quản lý @else <i class="fa fa-user-circle-o" aria-hidden="true"></i> Member @endif</td>
                        <td>{{ $customer->phone }}</td>
                        <td>{{ $customer->address }}</td>
                    <td>
                        <a class="btn btn-xs bg-green dt-edit" href="#" data-toggle="modal" data-target="#suathanhvien"><i class="fa fa-check-square"></i></a>
                        <a class="btn btn-xs bg-red dt-delete" href="#" data-toggle="modal" data-target="#xoathanhvien"><i class="fa fa-trash-o"></i></a>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                </table>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>
<!-- /.content-wrapper -->
<!-- Add the sidebars background. This div must be placed immediately after the control sidebar -->

<!--- Modal -->
<form action="{{ route('admin/themThanhVien') }}" method="post">
@csrf
<div class="modal fade" id="themthanhvien" style="display: none;">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thêm mới một thành viên</h4>
        </div>
        <div class="modal-body">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Tên thành viên</label>
                    <input type="text" class="form-control" required name="tenthanhvien">
                </div>
                <div class="form-group">
                    <label for="">Giới tính</label>
                    <select name="gioitinh" id="gioitinh" class="form-control" required>
                        <option value="1">Nam</option>
                        <option value="0">Nữ</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Địa chỉ email</label>
                    <input type="text" class="form-control" required name="email">
                </div>
                <div class="form-group">
                    <label for="">Số điện thoại</label>
                    <input type="text" class="form-control" required name="sodienthoai">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Mật khẩu đăng nhập</label>
                    <input type="text" class="form-control" required name="matkhau">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Quyền hạn trên hệ thống</label>
                    <select name="quyenhan" id="quyenhan" class="form-control" required>
                        <option value="1">Người quản lý</option>
                        <option value="0">Thành viên bình thường</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Địa chỉ sinh sống</label>
                    <input type="text" class="form-control" required name="diachi">
                </div>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-success">Thêm mới thành viên</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->

<!--- Modal -->
<form action="{{ route('admin/suaThanhVien') }}" method="post">
@csrf
<div class="modal fade" id="suathanhvien" style="display: none;">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Sửa thông tin thành viên</h4>
        </div>
        <div class="modal-body">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Tên thành viên</label>
                    <input type="hidden" class="form-control" required name="idthanhviensua" id="idthanhviensua">
                    <input type="text" class="form-control" required name="tenthanhviensua" id="tenthanhviensua">
                </div>
                <div class="form-group">
                    <label for="">Giới tính</label>
                    <select name="gioitinhsua" id="gioitinhsua" class="form-control" required>
                        <option value="1">Nam</option>
                        <option value="0">Nữ</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Địa chỉ email</label>
                    <input type="text" class="form-control" required name="emailsua" id="emailsua">
                </div>
                <div class="form-group">
                    <label for="">Số điện thoại</label>
                    <input type="text" class="form-control" required name="sodienthoaisua" id="sodienthoaisua">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Mật khẩu đăng nhập</label>
                    <input type="text" class="form-control" name="matkhausua" id="matkhausua">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Quyền hạn trên hệ thống</label>
                    <select name="quyenhansua" id="quyenhansua" class="form-control" required>
                        <option value="1">Người quản lý</option>
                        <option value="0">Thành viên bình thường</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Địa chỉ sinh sống</label>
                    <input type="text" class="form-control" required name="diachisua" id="diachisua">
                </div>
            </div>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-success">Sửa thông tin thành viên</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->


<!--- Modal -->
<form action="{{ route('admin/xoaThanhVien') }}" method="post">
@csrf
<div class="modal fade" id="xoathanhvien" style="display: none;">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Thông báo</h4>
        </div>
        <div class="modal-body">
        <input type="hidden" id="idthanhvien" name="idthanhvien">
        <label for="exampleInputEmail1">Bạn có chắc chắn muốn xóa thành viên này?</label>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
        <button type="submit" class="btn btn-danger">Xóa thành viên</button>
        </div>
    </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
</form>
<!--- End Modal -->
<script>
    $(function () {
        $('#khachhang').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false,
        "searching": true,
        })
    })
</script>

<script>
    //Edit row buttons
    $('.dt-edit').each(function () {
        $(this).on('click', function(evt){
        $this = $(this);
        var dtRow = $this.parents('tr');
        for(var i=0; i < dtRow[0].cells.length; i++){
            console.log(dtRow[0].cells[i].innerHTML);
            $('#idthanhviensua').val(dtRow[0].cells[0].innerHTML);
            $('#tenthanhviensua').val(dtRow[0].cells[1].innerHTML);
            $('#emailsua').val(dtRow[0].cells[2].innerHTML);
            if($.trim(dtRow[0].cells[3].innerHTML) == 'Nữ') {
                $('#gioitinhsua').val(0).change();
            }
            else {
                $('#gioitinhsua').val(1).change();
            }
            $('#sodienthoaisua').val(dtRow[0].cells[5].innerHTML);
            $('#diachisua').val(dtRow[0].cells[6].innerHTML);
        }
        });
    });
    
    $('.dt-delete').each(function () {
        $(this).on('click', function(evt){
        $this = $(this);
        var dtRow = $this.parents('tr');
        for(var i=0; i < dtRow[0].cells.length; i++){
            //console.log(dtRow[0].cells[i].innerHTML);
            $('#idthanhvien').val(dtRow[0].cells[0].innerHTML);
        }
        });
    });
</script>

<script src="dist/js/bootstrap-notify.js"></script>
<script>
    @if(count($errors) > 0)
    @foreach($errors->all() as $err)
    $.notify('<strong>Lỗi:</strong> {{ $err }}',
    { allow_dismiss: false,
      placement:
      {
        from: "top",
        align: "right"
      },
      type: "danger",
      delay: 7000,
    });
    @endforeach
    @endif

    @if(Session::has('thongbao'))
      $.notify('<strong>Thành công:</strong> {{ Session::get('thongbao') }}',
      { allow_dismiss: false,
        placement:
        {
          from: "top",
          align: "right"
        },
        type: "info",
        delay: 7000,
      });
    @endif

</script>
@endsection('content')